# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
import base64
import os
import requests
import uuid
import time


def scroll_into_view(driver, element):
    driver.execute_script("arguments[0].scrollIntoView(true);", element)
    time.sleep(2)


class Helpers(object):

    def __init__(self, parent):
        self.parent = parent
        self.icon_path = os.path.join(os.getcwd(), "resources", "icons")
        self.tmp_path = os.path.join(os.getcwd(), "dist", "tmp")

        if not os.path.exists(self.tmp_path):
                os.makedirs(self.tmp_path)

    def _icon_to_base64(self, icon_path):
        with open(icon_path, "rb") as icon_file:
            encoded_string = base64.b64encode(icon_file.read())
        return encoded_string

    def icon_to_base64(self, icon_name):
        icon_path = os.path.join(self.icon_path, icon_name)
        return self._icon_to_base64(icon_path)

    def remote_icon_to_base64(self, url):
        remote_icon_name = "test_{}".format(str(uuid.uuid4())[-10:])
        remote_icon_path = os.path.join(self.tmp_path, remote_icon_name)
        with open(remote_icon_path, 'wb') as remote_icon_file:
            response = requests.get(url, stream=True)

            if not response.ok:
                return

            for block in response.iter_content(1024):
                remote_icon_file.write(block)

        return self._icon_to_base64(remote_icon_path)
