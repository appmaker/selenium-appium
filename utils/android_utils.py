from appium.webdriver.common.touch_action import TouchAction


def uninstall_app(driver, app_xpath):
    icons = driver.find_elements_by_xpath(app_xpath)
    action = TouchAction(driver)
    while len(icons) > 0:
        action.press(icons[0]).wait(500).perform()
        remove_elem = driver.find_element_by_id("com.sec.android.app.launcher:id/home_edit_bar_delete")
        action.move_to(remove_elem).release().perform()
        icons = driver.find_elements_by_xpath(app_xpath)
