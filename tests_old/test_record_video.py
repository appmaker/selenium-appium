import time
import os
from seleniumtid.test_cases import SeleniumTestCase
from seleniumtid.pageelements import PageElement
from pageobjects.applications_list import ApplicationsListPageObject
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile


class RecordVideo(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(RecordVideo, self).setUp()

    def test_record(self):
        self.driver.get("http://pypi.python.org/pypi/selenium")
        self.driver.find_element_by_partial_link_text("selenium-2").click()

#===============================================================================
#    def test_pass_open_app(self):
#        app_name = "onewordniceappname "
#
#        applist_page = ApplicationsListPageObject()
#        applist_page.open()
#
#        handles = set(self.driver.window_handles)
#        applist_page.open_app(app_name)
#        new_handles = set(self.driver.window_handles)
#
#        app_tab = list(new_handles - handles)[0]
#        self.driver.switch_to_window(app_tab)
#===============================================================================

#===============================================================================
#    def test_fail_check_version_scope(self):
#        app_name = "onewordniceappname "
#
#        applist_page = ApplicationsListPageObject()
#        applist_page.open()
#
#        app_version = applist_page.get_app_version(app_name)
#        app_version_scope = PageElement(By.CSS_SELECTOR, 'dd', parent=app_version)
#        app_version_scope.scroll_element_into_view()
#        text = app_version_scope.element().text
#        self.assertTrue("Publicly" == text, "Incorrect scope found: {}  Expected: {}".format(text, "Publicly"))
#        app_id = applist_page.get_app_id(app_name)
#        app_id_number = PageElement(By.CSS_SELECTOR, 'dd', parent=app_id)
#        app_id_number.scroll_element_into_view()
#        text = app_id_number.element().text
#        self.assertTrue("2.0" == text, "Incorrect version number found: {}  Expected: {}".format(text, "2.0"))
#===============================================================================

    def tearDown(self):
        time.sleep(40)
        super(RecordVideo, self).tearDown()
