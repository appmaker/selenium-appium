from seleniumtid.test_cases import SeleniumTestCase
from pageobjects.applications_list import ApplicationsListPageObject
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from seleniumtid.visual_test import VisualTest
from seleniumtid import selenium_driver


class VisualPOC(SeleniumTestCase):

    def setUp(self):
        super(VisualPOC, self).setUp()

        # Playing a lil bit with screensizes. By this, we could avoid the problem of taking
        # baseline images from our local machine and compare them with the ones taken from
        # the GRID.

        # self.driver.set_window_size(1920, 1080)
        # # Measure the difference between the actual document width and the
        # # desired viewport width so we can account for scrollbars:
        # measured = self.driver.execute_script("
        #     return {width: document.body.clientWidth, height: document.body.clientHeight};")
        # delta = 1920 - measured['width']
        # self.driver.set_window_size(1920 + delta, 1080)

    def test_open_app(self):
        app_name = "hackedmaps"
        the_file = "screenshot_{}".format(selenium_driver.visual_number)
        map_viewer = (By.CSS_SELECTOR, '.mh5_Map.mh5_hwacc_viewer')

        applist_page = ApplicationsListPageObject()
        applist_page.open()

        handles = set(self.driver.window_handles)
        applist_page.open_app(app_name)
        new_handles = set(self.driver.window_handles)

        app_tab = list(new_handles - handles)[0]
        self.driver.switch_to_window(app_tab)
        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located(map_viewer))

        # Remember. First time (visualtests_save: false) to get the baseline pic. Once we have it,
        # run the test with (visualtests_save: true) to compare with that baseline pic within the
        # given threshold
        VisualTest().assertScreenshot(map_viewer[1], the_file, "", threshold=0)
