import os
import time
from seleniumtid.test_cases import SeleniumTestCase
from seleniumtid.pageelements import PageElement
from pageobjects.main_page import MainPageObject
from pageobjects.applications_list import ApplicationsListPageObject
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By


class ApplicationsList(SeleniumTestCase):

    def setUp(self):
        super(ApplicationsList, self).setUp()

    def test_open_app(self):
        app_name = "hackedmaps"
        map_viewer = (By.CSS_SELECTOR, '.mh5_Map.mh5_hwacc_viewer')

        applist_page = ApplicationsListPageObject()
        applist_page.open()

        handles = set(self.driver.window_handles)
        applist_page.open_app(app_name)
        new_handles = set(self.driver.window_handles)

        app_tab = list(new_handles - handles)[0]
        self.driver.switch_to_window(app_tab)
        found = WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located(map_viewer))
        self.assertTrue(found, "Element was not found")

    def test_open_app_by_link(self):
        app_name = "juego"
        play_btn = (By.CSS_SELECTOR, 'div#main button.button')

        applist_page = ApplicationsListPageObject()
        applist_page.open()

        handles = set(self.driver.window_handles)
        applist_page.open_app_by_link(app_name)
        new_handles = set(self.driver.window_handles)

        app_tab = list(new_handles - handles)[0]
        self.driver.switch_to_window(app_tab)

        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located(play_btn))
        btn = self.driver.find_element(*play_btn)
        self.assertTrue(btn.text == "PLAY", "Button does not contain the proper text")

    def test_click_download_links(self):
        applist_page = ApplicationsListPageObject()
        applist_page.open()
        app_name = "todo"
        app_name_elem = PageElement(By.XPATH, applist_page.app_article_xpath.format(app_name))
        download_links = app_name_elem.element().find_elements_by_css_selector('div.packages a.complete')

        dl_texts = [dl.text for dl in download_links]
        expected_links = ['Chrome', 'Firefox']
        for link in expected_links:
            self.assertTrue(link in dl_texts, "Download link for {} was not found".format(link))

    def test_delete_app(self):
        created_time = time.time()
        app_name = "VerySimple.zip"
        app_icon = "icon_1.png"
        path = "{}/{}/".format(os.getcwd(), "resources")

        main_page = MainPageObject()
        main_page.open()
        app_name_field = '{}_{}'.format(app_name.split(".")[0].lower(), created_time)
        main_page.fill_app_name_field(app_name_field)
        main_page.fill_app_description_field("This is the first time I'm uploading an App")
        time.sleep(5)
        main_page.upload_file(path + "zips/", app_name)

        main_page.upload_icon(path + "icons/", app_icon)

        main_page.make_app.element().click()
        time.sleep(2)
        self.driver.back()

        # Delete the application clicking the Delete button
        applist_page = ApplicationsListPageObject()
        applist_page.open()

        time.sleep(2)
        applist_page.delete_app(app_name_field)

        # Verify the application is no longer available
        app_name_locator = (By.XPATH, applist_page.app_article_xpath.format(app_name_field))
        self.utils.wait_until_element_not_visible(app_name_locator)

    def test_check_version_scope(self):
        app_name = "webrtcdemo2"

        applist_page = ApplicationsListPageObject()
        applist_page.open()

        app_version = applist_page.get_app_version(app_name)
        app_version_scope = PageElement(By.CSS_SELECTOR, 'dd', parent=app_version)
        app_version_scope.scroll_element_into_view()
        text = app_version_scope.element().text
        self.assertTrue("Publicly" == text, "Incorrect scope found: {}  Expected: {}".format(text, "Publicly"))
        app_id = applist_page.get_app_id(app_name)
        app_id_number = PageElement(By.CSS_SELECTOR, 'dd', parent=app_id)
        app_id_number.scroll_element_into_view()
        text = app_id_number.element().text
        self.assertTrue("1.0" == text, "Incorrect version number found: {}  Expected: {}".format(text, "1.0"))

    def tearDown(self):
        time.sleep(2)
        super(ApplicationsList, self).tearDown()
