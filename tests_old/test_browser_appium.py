# -*- coding: utf-8 -*-
'''
(c) Copyright 2014 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
import sys
import os
sys.path.insert(1, "./")
from seleniumtid.test_cases import SeleniumTestCase
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from seleniumtid import selenium_driver
from seleniumtid.config_driver import ConfigDriver
from pageobjects.applications_list import ApplicationsListPageObject
import time


class DownloadApk(SeleniumTestCase):

    def setUp(self):
        # Run with 'conf/apk-properties.cfg'
        os.environ['Files_properties'] = 'conf/apk-properties.cfg'
        super(DownloadApk, self).setUp()

    def test_simulate_apk_dowload(self):
        app_name = "juego"
        play_btn = (By.CSS_SELECTOR, 'div#main button.button')

        applist_page = ApplicationsListPageObject()
        applist_page.open()

        handles = set(self.driver.window_handles)
        applist_page.open_app_by_link(app_name)
        new_handles = set(self.driver.window_handles)

        app_tab = list(new_handles - handles)[0]
        self.driver.switch_to_window(app_tab)

        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(play_btn))
        btn = self.driver.find_element(*play_btn)
        self.assertTrue(btn.text == "PLAY", "Button does not contain the proper text")
        time.sleep(10)

        # Create a second driver
        config = selenium_driver.config.deepcopy()
        config.set('Browser', 'browser', 'android')
        appium_driver = ConfigDriver(config).create_driver()
        self.addCleanup(appium_driver.quit)

        # Mobile: open book. DONT FORGET TO USE THE SECOND DRIVER TO DO DE WAIT
        book_title = "El Nombre de la Rosa"
        WebDriverWait(appium_driver, 60).until(EC.element_to_be_clickable((By.NAME, book_title))).click()
