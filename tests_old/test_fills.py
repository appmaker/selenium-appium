# -*- coding: utf-8 -*-
'''
(c) Copyright 2014 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

import time
from seleniumtid.test_cases import SeleniumTestCase
from pageobjects.main_page import MainPageObject


class FillFields(SeleniumTestCase):

    def test_fill_repo_location(self):
        self.text = "This is a test repo location"
        main_page = MainPageObject()
        main_page.open()
        main_page.fill_repo_location_field(self.text)

        time.sleep(5)

        self.assertScreenshot('body', 'repo filled')
        self.assertEquals(self.text, main_page.repo_location.text)

    def test_fill_branch(self):
        self.text = "This is a test branch"
        main_page = MainPageObject()
        main_page.open()
        main_page.fill_branch_field(self.text)

        time.sleep(5)

        self.assertScreenshot('body', 'branch field')
        self.assertEquals(self.text, main_page.branch.text)

    def test_fill_app_name(self):
        self.text = "This is a test AppName"
        main_page = MainPageObject()
        main_page.open()
        main_page.fill_app_name_field(self.text)

        time.sleep(5)

        self.assertScreenshot('body', 'App name')
        self.assertEquals(self.text, main_page.app_name.text)

    def test_fill_app_description(self):
        self.text = "This is a test App Description"
        main_page = MainPageObject()
        main_page.open()
        main_page.fill_app_description_field(self.text)

        time.sleep(5)

        self.assertScreenshot('body', 'App description')
        self.assertEquals(self.text, main_page.app_description.text)
