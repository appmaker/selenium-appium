import os
import time
from seleniumtid.test_cases import SeleniumTestCase
from pageobjects.main_page import MainPageObject
from pageobjects.published_app import PublishedAppPageObject


class Uploads(SeleniumTestCase):
    path = "{}/{}/".format(os.getcwd(), "resources")

    def setUp(self):
        super(Uploads, self).setUp()
        self.logger.info("Using path for zips: {}".format(self.path))

    def test_upload_app(self):
        app_name = "VerySimple.zip"
        app_icon = "icon_1.png"
        expected_published_message = "Your App is ready to use!"

        main_page = MainPageObject()
        main_page.open()

        app_name_str = "verysimple_{:.0f}".format(time.time())
        main_page.fill_app_name_field(app_name_str)
        main_page.fill_app_description_field("This is the first time I'm uploading an App")
        time.sleep(5)
        main_page.upload_file(self.path + "zips/", app_name)
        self.assertIn(app_name, main_page.upload_zip.element().get_attribute("value"))

        main_page.upload_icon(self.path + "icons/", app_icon)
        self.assertIn(app_icon, main_page.upload_image.element().get_attribute("value"))

        main_page.make_app.element().click()
        time.sleep(5)

        self.published_page = PublishedAppPageObject()
        self.assertEquals(self.published_page.title.element().text, expected_published_message)
        time.sleep(30)

    def tearDown(self):
        time.sleep(5)
        self.published_page.delete_app()
        super(Uploads, self).tearDown()
