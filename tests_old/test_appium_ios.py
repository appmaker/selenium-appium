# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
from seleniumtid.test_cases import AppiumTestCase
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import os
import time


class IosTestApp(AppiumTestCase):
    def setUp(self):
        os.environ['Files_properties'] = 'conf/examples/properties.cfg;conf/ios-properties.cfg'
        super(IosTestApp, self).setUp()

    def test_open_website(self):
        self.driver.switch_to.context("NATIVE_APP")
        url_input = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, "//UIAElement[@name='URL']")))
        url_input.click()
        url = "www.ivao.es"
        for c in url:
            k = self.driver.find_element_by_xpath("//UIAKey[@label='{}']".format(c))
            k.click()
        time.sleep(60)
        go_btn = self.driver.find_element_by_xpath("//UIAButton[@label='go']")
        go_btn.click()
        time.sleep(60)
        community_link = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, "//UIALink[@name='COMUNIDAD']")))
        community_link.click()
        tours_link = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, "//UIALink[@name='TOURS']")))
        tours_link.click()
        time.sleep(10)
