# -*- coding: utf-8 -*-
'''
(c) Copyright 2014 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

from seleniumtid.pageobjects.page_object import PageObject
from seleniumtid.pageelements import InputText, PageElement
from selenium.webdriver.common.by import By


class MainPageObject(PageObject):

    def init_page_elements(self):
        self.repo_location = InputText(By.NAME, 'repo')
        self.branch = InputText(By.NAME, 'tag')
        self.pull_from_git = PageElement(By.ID, 'btn-pull')
        self.upload_zip = PageElement(By.ID, 'apppackage')
        self.upload_image = PageElement(By.ID, 'appicon')
        self.app_name = InputText(By.ID, 'appname')
        self.app_description = InputText(By.ID, 'appdesc')
        self.make_app = PageElement(By.CSS_SELECTOR, 'button.ready-btn')

    def open(self):
        self.driver.get(self.config.get('Common', 'url'))

    def fill_repo_location_field(self, text):
        self.repo_location.text = text

    def fill_branch_field(self, text):
        self.branch.text = text

    def fill_app_name_field(self, text):
        self.app_name.element().clear()
        self.app_name.text = text

    def fill_app_description_field(self, text):
        self.app_description.element().clear()
        self.app_description.text = text

    def upload_file(self, path, file_name):
        self.upload_zip.element().send_keys("{}{}".format(path, file_name))

    def upload_icon(self, path, file_name):
        self.upload_image.element().send_keys("{}{}".format(path, file_name))
