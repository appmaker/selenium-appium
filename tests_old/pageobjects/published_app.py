# -*- coding: utf-8 -*-
'''
(c) Copyright 2014 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
from seleniumtid.pageobjects.page_object import PageObject
from seleniumtid.pageelements import PageElement
from selenium.webdriver.common.by import By


class PublishedAppPageObject(PageObject):

    def init_page_elements(self):
        self.title = PageElement(By.CSS_SELECTOR, 'h2.appSuperTitle')
        self.app_icon = PageElement(By.CSS_SELECTOR, 'img.app-icon')
        self.app_name = PageElement(By.CSS_SELECTOR, 'h1.app-title a')
        self.app_description = PageElement(By.XPATH, 'div[@class="desc"][1]')
        self.delete_app_action = PageElement(By.CSS_SELECTOR, 'section.actions a.btn.icon')
        self.error_reason = PageElement(By.CSS_SELECTOR, '#content div.container p:nth-child(2)')

    def delete_app(self):
        self.delete_app_action.element().click()
