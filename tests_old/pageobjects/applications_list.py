# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
from seleniumtid.pageobjects.page_object import PageObject
from seleniumtid.pageelements import PageElement
from selenium.webdriver.common.by import By


class ApplicationsListPageObject(PageObject):

    def init_page_elements(self):
        self.title = PageElement(By.CSS_SELECTOR, 'h2.appSuperTitle')
        self.apps_list = PageElement(By.ID, 'container')
        self.app_title_xpath = '//article[@id="appid"]//h1/a[contains(text(), "{}")]'
        self.app_article_xpath = '//article[@id="appid"]//h1/a[contains(text(), "{}")]/ancestor::article'
        self.app_section_xpath = '//article[@id="appid"]//h1/a[contains(text(), "{}")]/ancestor::section'
        self.pkg_btn_xpath = '//a[contains(@class, "pkg") and (text()="{}")]'
        self.delete_app_action = PageElement('css selector', 'section.actions a.btn.icon')

    def delete_app(self, title):
        """Delete an application with the given name"""

        app_article = PageElement(By.XPATH, self.app_article_xpath.format(title))
        delete_btn = app_article.element().find_element_by_id('deleteButton')
        delete_btn.click()

    def open(self):
        self.logger.debug("Opening site: {}".format(self.config.get('Common', 'url') + '/apps'))
        self.driver.get(self.config.get('Common', 'url') + '/apps')

    def open_app(self, title):
        """Open the application whose name contains the word in title"""

        app_name = PageElement(By.XPATH, self.app_title_xpath.format(title))
        app_name.element().click()

    def open_app_by_link(self, title):
        """Open the application with the given name clicking the link in the description"""

        app_name = PageElement(By.XPATH, self.app_section_xpath.format(title))
        app_desc = app_name.element().find_element_by_css_selector('div.desc a')
        app_desc.click()

    def get_app_id(self, title):
        app_article = PageElement(By.XPATH, self.app_article_xpath.format(title))
        return PageElement(By.CSS_SELECTOR, 'dl.app-id', parent=app_article)

    def get_app_version(self, title):
        app_article = PageElement(By.XPATH, self.app_article_xpath.format(title))
        return PageElement(By.CSS_SELECTOR, 'dl.version', parent=app_article)

    def get_applications(self):
        return self.apps_list.element().find_elements(By.CSS_SELECTOR, 'article')
