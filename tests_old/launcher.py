from subprocess32 import Popen

processes = []
browsers = ['firefox', 'chrome']

for browser in browsers:
    p = Popen('Files_properties=conf/{}-properties.cfg nosetests tests/test_u*.py'.format(browser), shell=True)
    processes.append(p)

return_codes = {'{}'.format(p.pid): p.wait() for p in processes}
