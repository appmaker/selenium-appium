# Automate - Upload a new app with a name that already exits
import os
from seleniumtid.test_cases import SeleniumTestCase
from pageobjects.main_page import MainPageObject
from pageobjects.published_app import PublishedAppPageObject
from seleniumtid.jira import jira
from pageobjects.applications_list import ApplicationsListPageObject
from selenium.webdriver.common.by import By

import time


class Uploads(SeleniumTestCase):
    path = '{}/{}/'.format(os.getcwd(), 'resources')

    def setUp(self):
        super(Uploads, self).setUp()
        self.logger.info('Using path for zips: {}'.format(self.path))
        self.app_name = 'VerySimple.zip'
        self.app_icon = 'icon_1.png'
        self.app_name_field = self.app_name.split('.')[0].lower()

        expected_published_message = 'Your App is ready to use!'
        expected_published_app_title = self.app_name.split('.')[0].lower()

        main_page = MainPageObject()
        main_page.open()

        main_page.fill_app_name_field(self.app_name.split('.')[0])
        main_page.fill_app_description_field('This is the first time I am uploading an App')

        main_page.upload_file(self.path + 'zips/', self.app_name)
        self.assertIn(self.app_name, main_page.upload_zip.element().get_attribute('value'))

        main_page.upload_icon(self.path + 'icons/', self.app_icon)
        self.assertIn(self.app_icon, main_page.upload_image.element().get_attribute('value'))

        main_page.make_app.element().click()

        self.published_page = PublishedAppPageObject()
        self.assertEquals(self.published_page.title.element().text, expected_published_message)
        self.assertEquals(self.published_page.app_name.element().text, expected_published_app_title)

    # @jira('OWDAPPMAKE-95')
    def test_appmaker_95(self):
        expected_error_message = 'Cacheator error'
        expected_reason = 'Error, when trying to create the folderverysimple'

        main_page = MainPageObject()
        main_page.open()

        main_page.fill_app_name_field(self.app_name.split('.')[0])
        main_page.fill_app_description_field('This is the SECOND time I am uploading an App')

        main_page.upload_file(self.path + 'zips/', self.app_name)
        self.assertIn(self.app_name, main_page.upload_zip.element().get_attribute('value'))

        main_page.upload_icon(self.path + 'icons/', self.app_icon)
        self.assertIn(self.app_icon, main_page.upload_image.element().get_attribute('value'))

        main_page.make_app.element().click()
        
        published_page = PublishedAppPageObject()
        self.assertEquals(published_page.title.element().text, expected_error_message)
        self.assertEquals(published_page.error_reason.element().text, expected_reason)

    def tearDown(self):
        applist_page = ApplicationsListPageObject()
        applist_page.open()

        time.sleep(2)
        applist_page.delete_app(self.app_name_field)

        # Verify the application is no longer available
        app_name_locator = (By.XPATH, applist_page.app_article_xpath.format(self.app_name_field))
        self.utils.wait_until_element_not_visible(app_name_locator)
        super(Uploads, self).tearDown()
