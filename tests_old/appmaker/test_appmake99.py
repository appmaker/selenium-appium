# Automate - Access to web app from different desktop browsers, verify that it works when offline via appCache
import os
import time
import subprocess
from selenium import webdriver
from selenium.webdriver.common.by import By
from seleniumtid.test_cases import SeleniumTestCase
from seleniumtid.jira import jira
from tests.pageobjects.main_page import MainPageObject
from tests.pageobjects.published_app import PublishedAppPageObject
from tests.pageobjects.applications_list import ApplicationsListPageObject


class Uploads(SeleniumTestCase):
    path = '{}/{}/'.format(os.getcwd(), 'resources')

    """
    IMPORTANT NOTE: So far, we're unable to check somehow whether Chrome is 
    handling offline via AppCache or SW, so we'll just run this test for FFox
    """
    def setUp(self):
        os.environ['Files_properties'] = 'conf/firefox-properties-nosw.cfg'
        super(Uploads, self).setUp()
        # Make sure we have a cached version of the target app
        self.app_name = "onewordniceappname"
        self.applist_page = ApplicationsListPageObject()
        self.applist_page.open()

        self.handles = set(self.driver.window_handles)
        self.applist_page.open_app(self.app_name)
        new_handles = set(self.driver.window_handles)
        app_tab = list(new_handles - self.handles)[0]
        self.driver.switch_to_window(app_tab)

        self.driver.close()
        time.sleep(2)
        self.driver.switch_to_window(list(self.handles.intersection(new_handles))[0])

    # @jira('OWDAPPMAKE-99')
    def test_appmaker_99(self):
        cmd = 'sudo rfkill block wifi'

        process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        time.sleep(10)
        self.applist_page.open_app(self.app_name)
        time.sleep(10)
        new_handles = set(self.driver.window_handles)

        app_tab = list(new_handles - self.handles)[0]
        self.driver.switch_to_window(app_tab)

        html_tag = self.driver.find_element(By.CSS_SELECTOR, 'html')
        self.assertEquals(html_tag.get_attribute('manifest'), "manifest.appcache")

    def tearDown(self):
        cmd = 'sudo rfkill unblock wifi'
        process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        super(Uploads, self).tearDown()
