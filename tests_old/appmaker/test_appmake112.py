# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
from seleniumtid.test_cases import AppiumTestCase
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import os
import time


class IosPinHomescreen(AppiumTestCase):
    def setUp(self):
        os.environ['Files_properties'] = 'conf/examples/properties.cfg;conf/ios-properties.cfg'
        super(IosPinHomescreen, self).setUp()

    def test_open_website(self):
        self.driver.switch_to.context("NATIVE_APP")
        time.sleep(5)
        url_input = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, "//UIAElement[@name='URL']")))
        url_input.click()
        url_address = self.driver.find_element_by_xpath("//UIATextField[@name='Address']")
        url = "onewordniceappname.owd.tid.ovh"
        url_address.send_keys(url)
        go_btn = self.driver.find_element_by_name("Go")
        go_btn.click()
        time.sleep(2)
        self.logger.debug("CONTEXTS: {}".format(self.driver.contexts))
        add_notice = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, "//UIAStaticText[@name='Add to Home Screen']")))
        share_btn = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, "//UIAButton[@name='Share']")))
        share_btn.click()
        add_to_home_btn = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, "//UIACollectionCell[@name='Add to Home Screen']")))
        add_to_home_btn.click()
        time.sleep(2)
        self.logger.debug("Clearing text")
        clear_btn = self.driver.find_element_by_xpath("//UIAButton[@name='Clear text']")
        clear_btn.click()
        time.sleep(2)

        app_name = "NiceAppName"
        addi_btn = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, "//UIATextField[@name='']")))
        addi_btn.send_keys(app_name)
        time.sleep(1)
        add_btn = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, "//UIAButton[@name='Add']")))
        add_btn.click()
        time.sleep(1)

        # Check the launcher is present in the home screen
        icon = self.driver.find_element_by_xpath("//UIAButton[@name='NiceAppName']")
        self.assertTrue(icon is not None, "The application icon was not found")
        time.sleep(10)
