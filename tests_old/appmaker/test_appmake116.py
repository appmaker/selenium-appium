'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
#===============================================================================
# OWDAPPMAKE-117
#
# FxOS device - Open the already installed offline enabled web app
#===============================================================================

from gaiatest import GaiaTestCase
from OWDTestToolkit.utils.utils import UTILS
from OWDTestToolkit.apps.browser import Browser
from OWDTestToolkit import DOM
from seleniumtid.jira import jira
from seleniumtid import selenium_driver
from seleniumtid.test_cases import SeleniumTestCase


class test_main(GaiaTestCase, SeleniumTestCase):

    def setUp(self):

        # Set up child objects...
        GaiaTestCase.setUp(self)

        self.UTILS = UTILS(self)
        self.browser = Browser(self)
        self.app_name = "onewordniceappname"
        self.app_url = "http://onewordniceappname.owd.tid.ovh"
        self.add_locator = ('id', 'installer')
        self.install_btn = ('id', 'app-install-install-button')
        self.link = ('css selector', 'a[href="http://crossorigin.me"]')

    def tearDown(self):
        self.UTILS.app.uninstallApp(self.app_name)
        self.UTILS.reporting.reportResults()
        GaiaTestCase.tearDown(self)

    def test_run(self):
        self.data_layer.connect_to_wifi()

        self.browser.launch()
        self.browser.open_url(self.app_url)

        self.wait_for_element_displayed(*self.add_locator, timeout=15)
        add_btn = self.marionette.find_element(*self.add_locator)
        add_btn.tap()

        # Click the install button
        self.marionette.switch_to_frame()
        self.wait_for_element_displayed(*self.install_btn, timeout=10)
        install_btn = self.marionette.find_element(*self.install_btn)
        install_btn.tap()

        self.wait_for_element_displayed(*DOM.GLOBAL.modal_dialog_alert_ok, timeout=10)
        ok_btn = self.marionette.find_element(*DOM.GLOBAL.modal_dialog_alert_ok)
        ok_btn.tap()

        # Go to Homescreen and check the application has been installed
        self.UTILS.home.goHome()
        is_installed = self.UTILS.app.isAppInstalled(self.app_name)
        self.UTILS.test.test(is_installed, "Application {} has been properly installed".format(self.app_name))

        # Launch the application
        self.UTILS.app.launchAppViaHomescreen(self.app_name)
        self.UTILS.iframe.switchToFrame('src', self.app_name)
        cool_link = self.marionette.find_element(*self.link)
        self.UTILS.test.test(cool_link, "Found expected link in on-line mode")

        # Disable wifi to check off-line mode
        self.UTILS.home.goHome()
        self.data_layer.disable_wifi()

        # Launch the application again
        self.UTILS.app.launchAppViaHomescreen(self.app_name)
        self.UTILS.iframe.switchToFrame('src', self.app_name)
        cool_link = self.marionette.find_element(*self.link)
        self.UTILS.test.test(cool_link, "Found expected link in off-line mode")
