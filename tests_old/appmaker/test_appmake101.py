'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
#===============================================================================
# OWDAPPMAKE-101
#
# Uploaded app works fine when being offline, Firefox OS Browser
#===============================================================================

from gaiatest import GaiaTestCase
from OWDTestToolkit.utils.utils import UTILS
from OWDTestToolkit.apps.browser import Browser
from OWDTestToolkit import DOM
import time


class test_main(GaiaTestCase):

    def __init__(self, *args, **kwargs):
        kwargs['restart'] = True
        super(test_main, self).__init__(*args, **kwargs)

    def setUp(self):
        # Set up child objects...
        GaiaTestCase.setUp(self)

        self.UTILS = UTILS(self)
        self.browser = Browser(self)
        self.app_name = "fernando"
        self.app_url = "http://fernando.owd.tid.ovh"
        self.add_locator = ('id', 'installer')
        self.install_btn = ('id', 'app-install-install-button')
        self.exit_edit_btn = ('id', 'exit-edit-mode')

    def tearDown(self):
        self.UTILS.app.uninstallApp(self.app_name)
        exit_btn = self.marionette.find_element(*self.exit_edit_btn)
        exit_btn.tap()
        self.UTILS.reporting.reportResults()
        GaiaTestCase.tearDown(self)

    def test_run(self):
        self.data_layer.connect_to_cell_data()

        self.browser.launch()
        self.browser.open_url(self.app_url)

        self.wait_for_element_displayed(*self.add_locator, timeout=15)
        add_btn = self.marionette.find_element(*self.add_locator)
        add_btn.tap()

        # Click the install button
        self.marionette.switch_to_frame()
        self.wait_for_element_displayed(*self.install_btn, timeout=10)
        install_btn = self.marionette.find_element(*self.install_btn)
        install_btn.tap()

        self.wait_for_element_displayed(*DOM.GLOBAL.modal_dialog_alert_ok, timeout=10)
        ok_btn = self.marionette.find_element(*DOM.GLOBAL.modal_dialog_alert_ok)
        ok_btn.tap()

        # Go to Homescreen and check the application has been installed
        self.device.touch_home_button()
        is_installed = self.UTILS.app.isAppInstalled(self.app_name)
        time.sleep(10)
        self.UTILS.test.test(is_installed, "Application {} has been properly installed".format(self.app_name))

        # Disable network and open application
        self.data_layer.disable_cell_data()
        time.sleep(3)
        icon = self.UTILS.app.findAppIcon("fernando")
        icon.tap()

        # Check an element is present
        locator = ('id', 'config_accounttype')
        self.UTILS.iframe.switch_to_frame('src', 'fernando')
        self.wait_for_element_displayed(*locator, timeout=10)
        time.sleep(3)
        selector = self.marionette.find_element(*locator)
        self.UTILS.test.test(selector is not None, "Selector not found")
        time.sleep(3)
