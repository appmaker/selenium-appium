import os
from seleniumtid.test_cases import SeleniumTestCase
from pageobjects.main_page import MainPageObject
from pageobjects.published_app import PublishedAppPageObject
from seleniumtid.jira import jira
import time


class Uploads(SeleniumTestCase):
    path = '{}/{}/'.format(os.getcwd(), 'resources')

    def setUp(self):
        super(Uploads, self).setUp()
        self.logger.info('Using path for zips: {}'.format(self.path))

    # @jira('OWDAPPMAKE-91')
    def test_appmaker_91(self):
        app_name = 'VerySimple.zip'
        app_icon = 'icon_1.png'
        expected_published_message = 'Your App is ready to use!'
        expected_published_app_title = app_name.split('.')[0].lower()

        main_page = MainPageObject()
        main_page.open()

        main_page.fill_app_name_field(app_name.split('.')[0])

        main_page.upload_file(self.path + 'zips/', app_name)
        self.assertIn(app_name, main_page.upload_zip.element().get_attribute('value'))

        main_page.upload_icon(self.path + 'icons/', app_icon)
        self.assertIn(app_icon, main_page.upload_image.element().get_attribute('value'))
        time.sleep(5)

        main_page.make_app.element().click()

        self.published_page = PublishedAppPageObject()
        self.assertEquals(self.published_page.title.element().text, expected_published_message)
        self.assertEquals(self.published_page.app_name.element().text, expected_published_app_title)
        time.sleep(5)

    def tearDown(self):
        self.published_page.delete_app()
        super(Uploads, self).tearDown()
