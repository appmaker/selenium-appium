# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
from seleniumtid.test_cases import AppiumTestCase
from seleniumtid import selenium_driver
from seleniumtid.config_driver import ConfigDriver
from appium.webdriver.common.touch_action import TouchAction
import os
import time


class AndroidPinHomescreen(AppiumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/pin-properties.cfg'
        self.app_xpath = "//android.widget.TextView[@text='onewordniceappname.owd.tid.ovh']"
        super(AndroidPinHomescreen, self).setUp()

    def tearDown(self):
        self.toggle_airplane_mode()
        self.uninstall_app()
        super(AndroidPinHomescreen, self).tearDown()

    def test_open_browser(self):
        self.driver.get("http://onewordniceappname.owd.tid.ovh/")

        time.sleep(1)
        self.driver.switch_to.context("NATIVE_APP")
        self.driver.find_element_by_id("com.android.browser:id/star").click()
        self.driver.find_element_by_id("com.android.browser:id/folder").click()
        homescreen_xpath = "//android.widget.CheckedTextView[@resource-id='android:id/text1' and @text='Home screen']"
        homescreen_btn = self.driver.find_element_by_xpath(homescreen_xpath)
        homescreen_btn.click()

        self.driver.find_element_by_id("com.android.browser:id/OK").click()

        # Press the home key
        self.driver.press_keycode(3)

        # Check the launcher is present in the home screen
        icon = self.driver.find_elements_by_xpath(self.app_xpath)[-1]
        self.assertTrue(icon is not None, "The application icon was not found")
        time.sleep(3)
        # Disable networking
        self.toggle_airplane_mode()

        time.sleep(1)
        # Open the application again and check some elements are available
        icon.click()

        time.sleep(3)
        self.driver.switch_to.context("WEBVIEW_1")
        link_css = 'a[href="http://crossorigin.me"]'
        link = self.driver.find_element_by_css_selector(link_css)
        expected = "Cool link!"
        self.assertTrue(link.text == expected, "Error - Link text: {} Expected: {}".format(link.text, expected))

        self.driver.switch_to.context("NATIVE_APP")
        # Press the home key
        self.driver.press_keycode(3)

    def toggle_airplane_mode(self):
        action = TouchAction(self.driver)
        action.press(x=0.8, y=0.01).wait(500).move_to(x=0.8, y=0.4).perform()
        switch_xpath = "//android.widget.FrameLayout[contains(@content-desc, 'Airplane Mode')]"
        airplane_mode_switch = self.driver.find_element_by_xpath(switch_xpath)
        airplane_mode_switch.click()
        action.press(x=0.8, y=0.5).perform()

    def uninstall_app(self):
        icons = self.driver.find_elements_by_xpath(self.app_xpath)
        action = TouchAction(self.driver)
        while len(icons) > 0:
            action.press(icons[0]).wait(500).perform()
            remove_elem = self.driver.find_element_by_id("com.android.launcher:id/delete_target_text")
            action.move_to(remove_elem).release().perform()
            icons = self.driver.find_elements_by_xpath(self.app_xpath)
