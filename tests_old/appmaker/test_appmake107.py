# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
from seleniumtid.test_cases import AppiumTestCase
from seleniumtid import selenium_driver
from seleniumtid.config_driver import ConfigDriver
from appium.webdriver.common.touch_action import TouchAction
import os
import time


class AndroidPinHomescreen(AppiumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/pin-properties-chrome.cfg'
        self.app_xpath = "//android.widget.TextView[@text='onewordniceappname.owd.tid.ovh']"
        super(AndroidPinHomescreen, self).setUp()

    def tearDown(self):
        self.toggle_airplane_mode()
        self.driver.press_keycode(3)
        super(AndroidPinHomescreen, self).tearDown()

    def test_open_browser(self):
        self.driver.get(selenium_driver.config.get('Common', 'url'))
        time.sleep(3)

        self.driver.switch_to.context("WEBVIEW_1")
        link_css = 'a[href="./another-one.html"]'
        link = self.driver.find_element_by_css_selector(link_css)
        expected = "Go to another more place."
        self.assertTrue(link.text == expected, "****** Error - Link text: {} Expected: {}".format(link.text, expected))
        link.click()
        time.sleep(5)
        back_link_locator = 'a[href="./index.html"]'
        back_link = self.driver.find_element_by_css_selector(back_link_locator)
        expected = "Back"
        self.assertTrue(back_link.text == expected, "Error - Link text: {} Expected: {}".
                    format(back_link.text, expected))
        back_link.click()
        time.sleep(3)

        self.driver.switch_to.context("NATIVE_APP")

        self.toggle_airplane_mode()

        # Press the Back key
        self.driver.press_keycode(4)
        time.sleep(5)
        self.driver.switch_to.context("WEBVIEW_1")
        link = self.driver.find_element_by_css_selector(link_css)
        time.sleep(5)
        link.click()
        time.sleep(3)
        back_link = self.driver.find_element_by_css_selector(back_link_locator)
        expected = "Back"
        self.assertTrue(back_link.text == expected, "Error - Link text: {} Expected: {}".
                    format(back_link.text, expected))
        back_link.click()
        self.driver.switch_to.context("NATIVE_APP")
        # Press the home key
        self.driver.press_keycode(3)
        time.sleep(5)

    def toggle_airplane_mode(self):
        action = TouchAction(self.driver)
        action.press(x=0.8, y=0.01).wait(500).move_to(x=0.8, y=0.4).perform()
        settings_btn = self.driver.find_element_by_id("com.android.systemui:id/settings_button")
        settings_btn.click()
        time.sleep(1)
        switch_xpath = "//android.widget.FrameLayout[contains(@content-desc, 'Airplane Mode')]"
        airplane_mode_switch = self.driver.find_element_by_xpath(switch_xpath)
        airplane_mode_switch.click()
        action.press(x=0.8, y=0.5).perform()
