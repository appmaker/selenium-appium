# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
from seleniumtid.test_cases import AppiumTestCase
from seleniumtid import selenium_driver
from seleniumtid.config_driver import ConfigDriver
import os
import time


class AndroidPinHomescreen(AppiumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/pin-properties.cfg'
        super(AndroidPinHomescreen, self).setUp()

    def test_open_browser(self):
        self.driver.get("http://onewordniceappname.owd.tid.ovh/")

        time.sleep(5)
        self.driver.switch_to.context("NATIVE_APP")
        self.driver.find_element_by_id("com.android.browser:id/star").click()
        self.driver.find_element_by_id("com.android.browser:id/folder").click()
        homescreen_xpath = "//android.widget.CheckedTextView[@resource-id='android:id/text1' and @text='Home screen']"
        homescreen_btn = self.driver.find_element_by_xpath(homescreen_xpath)
        homescreen_btn.click()

        self.driver.find_element_by_id("com.android.browser:id/OK").click()

        # Press the home key
        self.driver.press_keycode(3)

        # Check the launcher is present in the home screen
        icon = self.driver.find_element_by_xpath("//android.widget.TextView[@text='onewordniceappname.owd.tid.ovh']")
        self.assertTrue(icon is not None, "The application icon was not found")
        time.sleep(10)
