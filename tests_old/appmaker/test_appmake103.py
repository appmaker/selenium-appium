# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
from seleniumtid.test_cases import AppiumTestCase
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.touch_actions import TouchActions
import os
import time
import subprocess


class TestAppmake103(AppiumTestCase):

    def setUp(self):
        os.environ[
            'Files_properties'] = 'conf/examples/properties.cfg;conf/ios-properties.cfg'
        super(TestAppmake103, self).setUp()
        self.url = "owd.tid.ovh/apps"
        self.app_locator = "//UIAStaticText[@label='heremaps']"
        self.grant_permission_locator = "//UIACollectionCell[@label='Allow']"
        self.ok_locator = "//UIAButton[@label='OK']"
        self.pages_btn_locator = "//UIAButton[@label='Pages']"
        self.close_app_locator = "//UIAButton[@label='Close HERE. Maps for Life.']"
        self.done_btn_locator = "//UIAButton[@label='Done']"
        self.search_locator = "//UIATextField[@value='Search for places']"

    def tearDown(self):
        subprocess.call('networksetup -setairportpower en0 on', shell=True)
        super(TestAppmake103, self).tearDown()

    def test_appmake103(self):
        self.driver.switch_to.context("NATIVE_APP")
        url_input = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, "//UIAElement[@name='URL']")))
        url_input.click()
        url_address = self.driver.find_element_by_xpath(
            "//UIATextField[@name='Address']")
        url_address.send_keys(self.url)
        go_btn = self.driver.find_element_by_name("Go")
        go_btn.click()

        app = WebDriverWait(self.driver, 20).until(
            EC.presence_of_element_located((By.XPATH, self.app_locator)))
        app.click()

        grant_permission = WebDriverWait(self.driver, 20).until(
            EC.presence_of_element_located((By.XPATH, self.grant_permission_locator)))
        grant_permission.click()

        ok_btn = WebDriverWait(self.driver, 20).until(
            EC.presence_of_element_located((By.XPATH, self.ok_locator)))
        ok_btn.click()

        pages_btn = WebDriverWait(self.driver, 20).until(
            EC.presence_of_element_located((By.XPATH, self.pages_btn_locator)))
        pages_btn.click()

        close_btn = WebDriverWait(self.driver, 20).until(
            EC.presence_of_element_located((By.XPATH, self.close_app_locator)))
        close_btn.click()

        done_btn = WebDriverWait(self.driver, 20).until(
            EC.presence_of_element_located((By.XPATH, self.done_btn_locator)))
        done_btn.click()

        subprocess.check_output('networksetup -setairportpower en0 off', shell=True)

        time.sleep(10)
        app = WebDriverWait(self.driver, 20).until(
            EC.presence_of_element_located((By.XPATH, self.app_locator)))
        app.click()

        ok_btn = WebDriverWait(self.driver, 20).until(
            EC.presence_of_element_located((By.XPATH, self.ok_locator)))
        ok_btn.click()

        WebDriverWait(self.driver, 20).until(
            EC.presence_of_element_located((By.XPATH, self.search_locator)))
