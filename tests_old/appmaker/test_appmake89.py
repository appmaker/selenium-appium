import os
from seleniumtid.test_cases import SeleniumTestCase
from pageobjects.main_page import MainPageObject
from pageobjects.published_app import PublishedAppPageObject
from seleniumtid.jira import jira
import time


class Uploads(SeleniumTestCase):
    path = '{}/{}/'.format(os.getcwd(), 'resources')

    def setUp(self):
        super(Uploads, self).setUp()
        self.logger.info('Using path for zips: {}'.format(self.path))

    #@jira('OWDAPPMAKE-89')
    def test_appmaker_89(self):
        app_name = 'VerySimple.zip'
        app_icon = 'icon_1.png'
        expected_error_message = 'Cacheator error'
        expected_reason = 'Missing Field: Please, introduce the App Name'

        main_page = MainPageObject()
        main_page.open()

        main_page.fill_app_name_field('')
        main_page.fill_app_description_field('This is the first time I am uploading an App')

        main_page.upload_file(self.path + 'zips/', app_name)
        self.assertIn(app_name, main_page.upload_zip.element().get_attribute('value'))

        main_page.upload_icon(self.path + 'icons/', app_icon)
        self.assertIn(app_icon, main_page.upload_image.element().get_attribute('value'))

        main_page.make_app.element().click()
        published_page = PublishedAppPageObject()
        self.assertEquals(published_page.title.element().text, expected_error_message)
        self.assertEquals(published_page.error_reason.element().text, expected_reason)
