# PRE-REQUISITES:
#  There is already a project created with a channel activated

# PROCEDURE
#  Tap on activated channel
#  Select from zip file and browse to choose a new file
#  Tap on Save

# EXPECTED RESULT
# The new file is uploaded successfully

import os
import time
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from seleniumtid.jira import jira
from utils.helpers import Helpers
from seleniumtid import selenium_driver


class OWDAppMake321(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        # App1 info
        self.channel_type = 'beta'
        self.app_name = "Test app name"
        self.app_descr = "This is a description for this app"
        self.icon = "icon_1.png"
        self.zip = "Usjpress.zip"
        
        self.app_locator = (By.CSS_SELECTOR, "div.uk-container.uk-container-center.uk-margin-top.uk-margin-large-bottom")
        self.wait_time = int(selenium_driver.config.get('Common', 'explicitly_wait'))

        # App2 info
        self.app_name2 = "New test app name"
        self.app_descr2 = "This is new description"
        self.icon2 = "icon_2.png"
        self.zip2 = "Here.zip"
        
        self.here_locator = (By.CSS_SELECTOR, "div.mh5_brandLogo")
        self.helpers = Helpers(self)

    @jira('OWDAPPMAKE-321')
    def test_upload_new_app_develp_and_metadata(self):
        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        projects = main_page.view_projects()

        # 2. Create a project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

        # 3. Activate a channel (i.e. BETA)
        channel = self.project.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        channel.fill_zip(self.zip)
        channel.click_upload_zip_next_button()

        channel.fill_manifest(self.app_name, self.app_descr, self.icon)
        channel.click_app_manifest_next_button()

        image_preview = channel.preview_icon.element().get_attribute('style').split("data:image/png;base64,")[-1]
        utils_preview = self.helpers.icon_to_base64(self.icon)
        self.assertIn(utils_preview, image_preview, msg="Base64 of uploaded image does NOT match the original base64")

        channel.fill_deployment()
        self.project = channel.click_save_channel()
        self.project.wait_for_channel_created(self.channel_type)

        # 4. Go to channel and open the deployed app
        channel = self.project.go_to_channel(self.channel_type)
        deploy = channel.get_deployment_url()
        self.logger.info("Deploy url: {}".format(deploy))

        handles = set(self.driver.window_handles)
        self.driver.execute_script('window.open("http://{}", "_blank");'.format(deploy))
        new_handles = set(self.driver.window_handles)
        app_tab = list(new_handles - handles)[0]
        self.driver.switch_to_window(app_tab)

        # 5. Check the app is properly hosted
        WebDriverWait(self.driver, self.wait_time).until(EC.presence_of_element_located(self.app_locator))
        self.driver.close()
        time.sleep(2)
        self.driver.switch_to_window(list(handles.intersection(new_handles))[0])
        channel.go_back()

        # 6. Go to channel and update its data
        channel = self.project.go_to_channel(self.channel_type)
        channel.upload_file(self.zip2)
        time.sleep(2)


        channel.edit_manifest_data(self.app_name2, self.app_descr2, self.icon2)

        image_preview = channel.preview_icon.element().get_attribute('style').split("data:image/png;base64,")[-1]
        utils_preview = self.helpers.icon_to_base64(self.icon2)
        time.sleep(2)
        #self.assertIn(utils_preview, image_preview, msg="Base64 of uploaded image does NOT match the original base64")

        self.project = channel.save_channel()

        channel = self.project.go_to_channel(self.channel_type)
        deploy = channel.get_deployment_url()
        self.logger.info("Deploy url: {}".format(deploy))

        handles = set(self.driver.window_handles)
        self.driver.execute_script('window.open("http://{}", "_blank");'.format(deploy))
        new_handles = set(self.driver.window_handles)
        app_tab = list(new_handles - handles)[0]
        self.driver.switch_to_window(app_tab)

        # Check the app is properly hosted
        WebDriverWait(self.driver, self.wait_time).until(EC.presence_of_element_located(self.here_locator))
        self.driver.close()
        time.sleep(2)
        self.driver.switch_to_window(list(handles.intersection(new_handles))[0])
        channel.go_back()

    def tearDown(self):
        self.project.wait_for_project_created()
        self.project.delete_project()
        super(self.__class__, self).tearDown()