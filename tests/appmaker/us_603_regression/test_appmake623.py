# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME: SC 1-4 Verify the description field

# SCENARIO
#   There is a project already created, no channels activated
#   Scenario: Try with valid file/options


# PROCEDURE
#   1- Tap on Activate a channel (ER1)
#   [Scenario]
#   2- Upload a zip file (ER2)
#   3- Tap on Next (ER3)
#   4- Add a name and description (ER4)
#   5- Tap on Next (ER5)


# EXPECTED RESULT
#   ER1. Step 1 menu is open. Next button is disabled
#   ER2. Once the file is uploaded, Next button is active
#   ER3. Step 2 menu is open. Next button is disabled
#   ER4. Once name and description (mandatory fields) are filled, Next button is active
#   ER5. Step 3 menu is open

import os
import time
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from seleniumtid.jira import jira
from utils.helpers import Helpers
from seleniumtid import selenium_driver


class OWDAppMake623(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        # App info
        self.channel_type = 'beta'
        self.app_name = "Test app name"
        self.app_descr = "This is a description for this app"
        self.icon = "icon_1.png"
        self.zip = "Here.zip"

        self.helpers = Helpers(self)
        self.wait_time = int(selenium_driver.config.get('Common', 'explicitly_wait'))
        self.expected_not_found_locator = (By.XPATH, '//h1[text()="404 Not Found"]')

    @jira('OWDAPPMAKE-623')
    def test_verify_description_field_long_descr_with_strange_chars(self):
        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        projects = main_page.view_projects()

        # 2. Create a project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

        # 3. Activate a channel (i.e. BETA)
        self.channel = self.project.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        self.channel.fill_zip(self.zip)
        self.channel.click_upload_zip_next_button()

        self.channel.fill_manifest(self.app_name, self.app_descr, self.icon)
        self.assertEquals(self.channel.app_description.text, self.app_descr, msg="App descriptions DO NOT MATCH")
        self.channel.click_app_manifest_next_button()

        image_preview = self.channel.preview_icon.element().get_attribute('style').split("data:image/png;base64,")[-1]
        utils_preview = self.helpers.icon_to_base64(self.icon)
        self.assertIn(utils_preview, image_preview, msg="Base64 of uploaded image does NOT match the original base64")

        time.sleep(2)
        self.driver.execute_script('''arguments[0].scrollIntoView;''', channel.go_back_button.element())
        time.sleep(2)
        channel.go_back()

    def tearDown(self):
        self.project.delete_project()
        super(self.__class__, self).tearDown()