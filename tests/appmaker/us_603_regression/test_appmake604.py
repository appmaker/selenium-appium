# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
#Cloned from OWDAPPMAKE-322
# TEST NAME: Create one project

# PROCEDURE
# 1. Open Appmaker Client
# 2. Click on start now (ER1)
# 3. Click on Project Name field and write something (ER2)
# 4. Tap on Create (ER3)

# EXPECTED RESULT
# ER1. User is taken to new screen with New Poject chart available
# ER2. Project chart is active when going on it and when typing something into Project Name field
# ER3. A new chart is created with the name typed by user


import os
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from seleniumtid.jira import jira

class OWDAppMake604(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

    @jira('OWDAPPMAKE-604')
    def test_create_new_project(self):
        main_page = StartPageObject()
        main_page.open()
        projects = main_page.view_projects()

        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = projects.create_new_project(project_name)
        self.project.wait_for_project_created()

    def tearDown(self):
        self.project.delete_project()
        super(self.__class__, self).tearDown()
