# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# Cloned from OWDAPPMAKE-319
# TEST NAME: Verify the default icon

# PROCEDURE
# - Create a new project and name it
# - Activate a channel
# - Select zip as the way to upload the app
# - Tap on Next
# - Name the app
# - Add a description
# - Upload an icon
# - Tap on Next
# - Select the way to deploy
# - Tap on Create

# EXPECTED RESULT
#   Verify that there is an icon given by default
#   Verify that when the channel is created the default icon appears correctly

import os
import time
import uuid
import re
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from seleniumtid.jira import jira
from utils.helpers import Helpers
from seleniumtid import selenium_driver


class OWDAppMake622(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        # App info
        self.channel_type = 'beta'
        self.app_name = "Test app name"
        self.app_descr = "This is a description for this app"
        self.icon_default = "default_beta.png"
        self.zip = "Here.zip"

        self.helpers = Helpers(self)
        self.here_locator = (By.CSS_SELECTOR, "div.mh5_brandLogo")
        self.wait_time = int(selenium_driver.config.get('Common', 'explicitly_wait'))

    @jira('OWDAPPMAKE-622')
    def test_default_icon(self):
        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        projects = main_page.view_projects()

        # 2. Create a project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

        # 3. Activate a channel (i.e. BETA)
        channel = self.project.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        channel.fill_zip(self.zip)
        channel.click_upload_zip_next_button()

        channel.fill_manifest(self.app_name, self.app_descr)
        channel.click_app_manifest_next_button()

        icon_style = channel.preview_icon.element().get_attribute('style')
        image_location = re.search(".*url\((.*)\);", icon_style).group(1)
        remote_base64 = self.helpers.remote_icon_to_base64(image_location)

        local_base64 = self.helpers.icon_to_base64(self.icon_default)
        self.assertEquals(local_base64, remote_base64, msg="Base64 of uploaded image does NOT match the original base64")

        channel.fill_deployment()
        self.project = channel.click_save_channel()
        self.project.wait_for_channel_created(self.channel_type)

        thumb_location = self.project.get_thumbnail_location(self.channel_type)
        thumb_base64 = self.helpers.remote_icon_to_base64(thumb_location)
        self.assertEquals(local_base64, thumb_base64, msg="Base64 of thumbnail does NOT match the original base64")

    def tearDown(self):
        self.project.delete_project()
        super(self.__class__, self).tearDown()
