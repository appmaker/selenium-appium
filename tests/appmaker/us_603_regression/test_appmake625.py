# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# Cloned from OWDAPPMAKE-475
# TEST NAME: Verify that the automatic scroll works when each Step is unblocked

# PROCEDURE
#   1- Tap on Activate a channel 
#   2- Upload a zip file 
#   3- Tap on Next (ER1)
#   4- Add a name and description
#   5- Tap on Next (ER2)


# EXPECTED RESULT
#   ER1. The new Step menu is open and the scroll down is done successfully to focus the screen in that menu Step2
#   ER2. The new Step menu is open and the scroll down is done successfully to focus the screen in that menu Step3
  
import os
import time
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from seleniumtid.jira import jira
from utils.helpers import Helpers
from seleniumtid import selenium_driver


class OWDAppMake625(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        # App info
        self.channel_type = 'beta'
        self.app_name = "Test app name"
        self.app_descr = "This is a description for this app"
        self.icon = "icon_1.png"
        self.zip = "Here.zip"
        self.helpers = Helpers(self)

        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        projects = main_page.view_projects()

        # 2. Create a project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

    def _get_y(self, element):
        return self.driver.execute_script('''
                                    function getY( oElement )
                                    {
                                        var iReturnValue = 0;
                                        while( oElement != null ) {
                                            iReturnValue += oElement.offsetTop;
                                            oElement = oElement.offsetParent;
                                        }
                                        return iReturnValue;
                                    };
                                    return getY(arguments[0]);
                                    ''', element)

    @jira('OWDAPPMAKE-625')
    def test_automatic_scroll_create_channel(self):

        # 3. Activate a channel (i.e. BETA)
        channel = self.project.activate_channel(self.channel_type)
        scroll_before_step_1 = self._get_y(channel.current_item_accordion.element())
        self.logger.info("Scroll before step_1: {}".format(scroll_before_step_1))

        # TODO - make sure zip method is selected
        channel.fill_zip(self.zip)
        channel.click_upload_zip_next_button()

        scroll_after_step_1 = self._get_y(channel.current_item_accordion.element())
        self.logger.info("Scroll after step_1: {}".format(scroll_after_step_1))
        self.assertGreater(scroll_after_step_1, scroll_before_step_1, msg="After clicking on Upload zip, the scroll DID NOT move")

        channel.fill_manifest(self.app_name, self.app_descr, self.icon)
        channel.click_app_manifest_next_button()
        scroll_after_step_2 = self._get_y(channel.current_item_accordion.element())
        self.logger.info("Scroll after step_2: {}".format(scroll_after_step_2))
        self.assertGreater(scroll_after_step_2, scroll_after_step_1, msg="After filling the manifest, the scroll DID NOT move")

        time.sleep(2)
        self.driver.execute_script('''arguments[0].scrollIntoView;''', channel.go_back_button.element())
        time.sleep(2)
        channel.go_back()

    def tearDown(self):
        self.project.delete_project()
        super(self.__class__, self).tearDown()