# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME: Select as icon for the app - invalid file

# DATASET
#   Correct zip file with valid files in it (with no icon) to be uploaded

# PROCEDURE
#   -Create a new project and name it
#   -Activate a channel
#   -Select zip as the way to upload the app
#   -Tap on Next
#   -Name the app
#   -Add a description
#   -Upload as an icon an invalid file


# EXPECTED RESULT
#   There is an error message to let user know that the file is not valid as an icon

import os
import time
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from seleniumtid.jira import jira
from utils.helpers import Helpers
from seleniumtid import selenium_driver


class OWDAppMake665(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        # App info
        self.channel_type = 'beta'
        self.app_name = "Test app name"
        self.app_descr = "This is a description for this app"
        self.wrong_icon = "wrong_icon.txt"
        self.zip = "Usjpress.zip"

        # self.expected_error_msg = u'La extensión del archivo es incorrecta.'
        self.expected_error_msg = u'The file extension is incorrect.'
        self.wait_time = int(selenium_driver.config.get('Common', 'explicitly_wait'))

    @jira('OWDAPPMAKE-665')
    def test_invalid_icon_create_channel(self):
        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        projects = main_page.view_projects()

        # 2. Create a project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

        # 3. Activate a channel (i.e. BETA)
        channel = self.project.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        channel.fill_zip(self.zip)
        channel.click_upload_zip_next_button()

        channel.fill_manifest(self.app_name, self.app_descr, self.wrong_icon)
        error_msg = WebDriverWait(self.driver, self.wait_time).until(EC.presence_of_element_located(
            channel.upload_icon_error_msg.locator))

        self.assertEquals(self.expected_error_msg.encode("utf8"), error_msg.text.encode("utf8"),
                          msg="Error message DOES NOT match the expected value. Expected [{}]. Got[{}]".\
                          format(self.expected_error_msg.encode("utf8"), error_msg.text.encode("utf8")))
        self.assertIn("disabled", channel.step2_next_button.element().get_attribute('class'),
                      msg="Next button is NOT disabled")

        time.sleep(2)
        self.driver.execute_script('''arguments[0].scrollIntoView;''', channel.go_back_button.element())
        time.sleep(2)
        channel.go_back()

    def tearDown(self):
        self.project.wait_for_project_created()
        self.project.delete_project()
        super(self.__class__, self).tearDown()
