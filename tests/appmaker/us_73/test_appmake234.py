# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME: SC 1-4 Verify the description field

# SCENARIO
#   Scenario 1: Long description
#   Scenario 2: Description with spaces
#   Scenario 3: Description with strange characters
#   Scenario 4: All previous scenarios
#   Scenario 5: Do not introduce any description

# PROCEDURE
#   -Create a new project and name it
#   -Activate a channel
#   -Select zip as the way to upload the app
#   -Tap on Next
#   -Name the app
#   -Add a description [Scenario]

# EXPECTED RESULT
#   ER1,2,3,4. The user can add the description of the app successfully.
#   ER5. There is an error message shown to user letting him know that the app description cannot be empty

import os
import time
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from seleniumtid.jira import jira
from utils.helpers import Helpers
from seleniumtid import selenium_driver


class OWDAppMake234(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        # App info
        self.channel_type = 'beta'
        self.app_name = "Test app name"
        self.app_descr = u'This is a very long description with strange characters. Here it goes:\
        Lórem ªºípsum dòlor sit ámet, consèctetûr @dipiscing elit!! Donec libéro lêo, porta ac mauris convallis,\
        {dignissim} efficitur lorem. @@Pellentesque dignissim, erat et vehicula feugiat, augue dui lobortis risus,\
        ut [efficitur] (diam) pu&&rus eu ~~ orci#. Integer mauris mi, venenatis vitae maximus ullamcorper, $%&/ççñ\
        commodo quis massa.'
        self.icon = "icon_1.png"
        self.zip = "Here.zip"

        self.helpers = Helpers(self)
        self.wait_time = int(selenium_driver.config.get('Common', 'explicitly_wait'))
        self.expected_not_found_locator = (By.XPATH, '//h1[text()="404 Not Found"]')

    @jira('OWDAPPMAKE-234')
    def test_verify_description_field_long_descr_with_strange_chars(self):
        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        projects = main_page.view_projects()

        # 2. Create a project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

        # 3. Activate a channel (i.e. BETA)
        self.channel = self.project.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        self.channel.fill_zip(self.zip)
        self.channel.click_upload_zip_next_button()

        self.channel.fill_manifest(self.app_name, self.app_descr, self.icon)
        self.assertEquals(self.channel.app_description.text, self.app_descr, msg="App descriptions DO NOT MATCH")
        self.channel.click_app_manifest_next_button()

        image_preview = self.channel.preview_icon.element().get_attribute('style').split("data:image/png;base64,")[-1]
        utils_preview = self.helpers.icon_to_base64(self.icon)
        self.assertIn(utils_preview, image_preview, msg="Base64 of uploaded image does NOT match the original base64")

        self.channel.fill_deployment()
        self.project = self.channel.click_save_channel()
        self.project.wait_for_channel_created(self.channel_type)

    def tearDown(self):
        self.project.delete_project()
        super(self.__class__, self).tearDown()