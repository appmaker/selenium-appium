# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME: Verify whether it is possible to change the icon once it has been previewed

# PROCEDURE
#   -Create a new project and name it
#   -Activate a channel
#   -Select zip as the way to upload the app
#   -Tap on Next
#   -Name the app
#   -Add a description
#   -Upload an icon
#   -Once it is previewed, change it to a different one


# EXPECTED RESULT
#   It is possible to change the icon.
#   The preview of the new once uploaded is shown fine

import os
import time
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from seleniumtid.jira import jira
from utils.helpers import Helpers
from seleniumtid import selenium_driver


class OWDAppMake236(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        # App info
        self.channel_type = 'beta'
        self.app_name = "Test app name"
        self.app_descr = "This is a description for this app"
        self.icon_1 = "icon_1.png"
        self.icon_2 = "icon_2.png"
        self.zip = "Here.zip"

        self.helpers = Helpers(self)
        self.wait_time = int(selenium_driver.config.get('Common', 'explicitly_wait'))

    @jira('OWDAPPMAKE-236')
    def test_change_icon_preview_channel_create(self):
        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        projects = main_page.view_projects()

        # 2. Create a project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

        # 3. Activate a channel (i.e. BETA)
        channel = self.project.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        channel.fill_zip(self.zip)
        channel.click_upload_zip_next_button()

        channel.fill_manifest(self.app_name, self.app_descr, self.icon_1)
        time.sleep(3)
        image_preview = channel.preview_icon.element().get_attribute('style').split("data:image/png;base64,")[-1]
        utils_preview = self.helpers.icon_to_base64(self.icon_1)
        self.assertIn(utils_preview, image_preview, msg="Base64 of uploaded image #1 does NOT match the original base64")

        # Change the icon
        channel.upload_icon(self.icon_2)
        image_preview = channel.preview_icon.element().get_attribute('style').split("data:image/png;base64,")[-1]
        utils_preview = self.helpers.icon_to_base64(self.icon_2)
        self.assertIn(utils_preview, image_preview, msg="Base64 of uploaded image #2 does NOT match the original base64")
        channel.click_app_manifest_next_button()

        time.sleep(2)
        self.driver.execute_script('''arguments[0].scrollIntoView;''', channel.go_back_button.element())
        time.sleep(2)
        channel.go_back()

    def tearDown(self):
        self.project.delete_project()
        super(self.__class__, self).tearDown()
