# encoding: utf-8
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
#===============================================================================
# Android device - Open the already installed offline enabled web app
#===============================================================================

import os
import time
import uuid
import sys
sys.path.insert(1, "./")

from appium.webdriver.connectiontype import ConnectionType
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

from seleniumtid.jira import jira
from seleniumtid import selenium_driver
from seleniumtid.test_cases import AppiumTestCase
from seleniumtid.config_driver import ConfigDriver
from page_objects.start_page import StartPageObject
from seleniumtid.pageelements import PageElement
from utils.helpers import Helpers
import utils.android_utils as android_utils
import logging


class test_main(AppiumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-android-properties.cfg'
        super(test_main, self).setUp()

        # App info
        self.channel_type = 'production'
        self.app_name = "Test app name"
        self.app_descr = "This is a description for this app"
        self.icon = "icon_1.png"
        self.zip = "Here.zip"

        self.helpers = Helpers(self)
        logging.config.fileConfig('conf/logging.conf')
        self.logger = logging.getLogger(self.__class__.__name__)

    def tearDown(self):
        self.appium_driver.press_keycode(3)
        self.appium_driver.set_network_connection(ConnectionType.ALL_NETWORK_ON)
        android_utils.uninstall_app(self.appium_driver, self.app_xpath)
        self.project.delete_project()
        super(test_main, self).tearDown()

    def _create_a_production_channel(self):
        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        projects = main_page.view_projects()

        # 2. Create a project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

        # 3. Activate a channel (i.e. BETA)
        channel = self.project.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        channel.fill_zip(self.zip)
        channel.click_upload_zip_next_button()

        channel.fill_manifest(self.app_name, self.app_descr, self.icon)
        channel.click_app_manifest_next_button()

        image_preview = channel.preview_icon.element().get_attribute('style').split("data:image/png;base64,")[-1]
        utils_preview = self.helpers.icon_to_base64(self.icon)
        self.assertIn(utils_preview, image_preview, msg="Base64 of uploaded image does NOT match the original base64")

        channel.fill_deployment()
        self.app_url = 'http://' + channel.deploy_url_copy.element().text + channel.deploy_url_copy_host.element().text
        self.project = channel.click_save_channel()
        self.project.wait_for_channel_created(self.channel_type)

    def _add_to_homescreen(self):
        # Create a second driver to run browser in Android device
        config = selenium_driver.config.deepcopy()
        config.set('Browser', 'browser', 'android')
        self.appium_driver = ConfigDriver(config).create_driver()
        self.addCleanup(self.appium_driver.quit)
        self.logger.debug("Opening %s in the second driver" % self.app_url)
        self.appium_driver.get(self.app_url)

        # Pin the application to the home screen
        self.appium_driver.switch_to.context("NATIVE_APP")
        self.appium_driver.find_element_by_id("com.android.chrome:id/menu_button").click()
        time.sleep(3)
        add_to_home = "//android.widget.TextView[@resource-id='com.android.chrome:id/menu_item_text' "\
                       "and @text='Añadir a pantalla de inicio']"
        self.appium_driver.find_element_by_xpath(add_to_home).click()
        self.appium_driver.find_element_by_id("android:id/button1").click()
        time.sleep(2)

    def test_run(self):
        self._create_a_production_channel()
        self._add_to_homescreen()

        # Press the home key
        self.appium_driver.press_keycode(3)

        self.app_xpath = '//android.widget.TextView[@content-desc="HERE Maps"]'
        WebDriverWait(self.appium_driver, 15).until(EC.visibility_of_element_located((By.XPATH, self.app_xpath)))
        self.appium_driver.find_element_by_xpath(self.app_xpath).click()

        search_xpath = '//android.widget.EditText[@content-desc="Search for places"]'
        WebDriverWait(self.appium_driver, 15).until(EC.visibility_of_element_located((By.XPATH, search_xpath)))

        self.appium_driver.find_element_by_xpath(search_xpath)
        self.appium_driver.set_network_connection(ConnectionType.AIRPLANE_MODE)

        # Close the application
        self.appium_driver.press_keycode(187)
        time.sleep(2)
        self.appium_driver.find_element_by_id("com.android.systemui:id/recents_RemoveAll_button_kk").click()

        # Press the home key
        self.appium_driver.press_keycode(3)

        # Open the application again and check the search bar is there
        self.appium_driver.find_element_by_xpath(self.app_xpath).click()
        search_xpath = '//android.widget.EditText[@content-desc="Connect to internet to search"]'
        WebDriverWait(self.appium_driver, 15).until(EC.visibility_of_element_located((By.XPATH, search_xpath)))
        self.appium_driver.find_element_by_xpath(search_xpath)
