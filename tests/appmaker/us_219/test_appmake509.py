# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME:  Rename a project with a name already in use

# PROCEDURE
# 1. Go to project list view
# 2. Tap on the arrow on the right up corner of a chart (ER1)
# 3. Tap on Project name to rename it (ER2)
# 4. Tap on the name and modify it, choosing a name already existing for another project (ER3)
# 5. Tap on Save (ER4)

# EXPECTED RESULT
# ER1. A menu with an option to rename the project is shown
# ER2. A new window is open, the current name of the project is shown
# ER3/ER4 At some point in these steps, there should be a warning letting user know that the name already exists

import time
import os
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from seleniumtid.jira import jira
from seleniumtid import selenium_driver


class OWDAppMake509(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()
        # self.expected_msg = "Nombre del Proyecto ya se ha utilizado"
        self.expected_error_msg = 'Project name has already been taken'
        self.wait_time = int(selenium_driver.config.get('Common', 'explicitly_wait'))

        main_page = StartPageObject()
        main_page.open()
        projects = main_page.view_projects()

        # Create two projects: The second one will be edited with the name of the first one.
        self.project_name_1 = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project_1 = projects.create_new_project(self.project_name_1)
        self.project_1.wait_for_project_created()

        self.project_name_2 = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project_2 = projects.create_new_project(self.project_name_2)
        self.project_2.wait_for_project_created()

    @jira("OWDAPPMAKE-509")
    def test_rename_project_with_name_already_used(self):
        self.project_2.select_edit()
        self.project_2.project_edit_input.element().clear()
        self.project_2.project_edit_input.text = self.project_name_1

        error_msg = WebDriverWait(self.driver, self.wait_time).until(
            EC.presence_of_element_located(self.project_2.project_edit_error_msg.locator))

        self.assertEquals(self.expected_error_msg, error_msg.text, msg="Expected error message NOT SHOWN")
        self.project_2.project_edit_cancel.element().click()

        self.assertNotEquals(self.project_2.project_card.element().get_attribute(
            "data-project-name"), self.project_name_1, msg='Project #2 have the same name than Project #1')
        self.assertEquals(self.project_2.project_card.element().get_attribute(
            "data-project-name"), self.project_name_2)

        self.assertNotEquals(self.project_2.project_card.element().find_element(
            By.CSS_SELECTOR, "h4").text, self.project_name_1)
        self.assertEquals(self.project_2.project_card.element().find_element(
            By.CSS_SELECTOR, "h4").text, self.project_name_2)
        time.sleep(2)

    def tearDown(self):
        self.project_1.delete_project()
        self.project_2.delete_project()
        super(self.__class__, self).tearDown()
