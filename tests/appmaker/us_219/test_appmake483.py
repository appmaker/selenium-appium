# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME:  Move from one channel to the other when updating a channel

# PRERREQUISITES: There should be a project already created

# DATASET
#   Scenario 1: Both channels activated
#   Scenario 2: One channel activated


# PROCEDURE
#   [Choose scenario]
#   1. Tap on any of the activated channels
#   2. On edit view tap on the other channel name, on the left column (ER1)
#   3. Tap again on the other channel name (ER2)


# EXPECTED RESULT
#   ER1. The other channel is open in edit view, all its details appear ok
#   ER2. The channel is open in edit view,  all its details appear ok

import os
import time
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from seleniumtid.jira import jira
from utils.helpers import Helpers
from seleniumtid import selenium_driver


class OWDAppMake483(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        # App info
        self.channel_type = 'beta'
        self.app_name = "Test app name"
        self.app_descr = "This is a description for this app"
        self.icon = "icon_1.png"
        self.zip = "Here.zip"
        self.helpers = Helpers(self)
        self.deploy_url = "test_deploy_{}".format(str(uuid.uuid4())[-6:])

        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        self.projects = main_page.view_projects()

        # 2. Create a project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = self.projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

    def _activate_channel(self, deploy_url=None):
        channel = self.project.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        channel.fill_zip(self.zip)
        channel.click_upload_zip_next_button()

        channel.fill_manifest(self.app_name, self.app_descr, self.icon)
        channel.click_app_manifest_next_button()

        image_preview = channel.preview_icon.element().get_attribute('style').split("data:image/png;base64,")[-1]
        utils_preview = self.helpers.icon_to_base64(self.icon)
        self.assertIn(utils_preview, image_preview, msg="Base64 of uploaded image does NOT match the original base64")

        channel.fill_deployment(deploy_url)
        self.project = channel.click_save_channel()
        self.project.wait_for_channel_created(self.channel_type)

    @jira('OWDAPPMAKE-483')
    def test_update_move_channels_one_channel(self):
        self._activate_channel(self.deploy_url)

        beta_channel = self.project.go_to_channel(self.channel_type)
        production_channel = beta_channel.switch_tabs(target_po='create')

        current_url = self.driver.execute_script("""return window.location.href;""")
        active_tab = production_channel.get_active_tab()
        self.assertIn(production_channel.channel_type, active_tab.get_attribute('href'), 
                        msg="Current tab is not {}".format(production_channel.channel_type.upper()))
        self.assertIn(production_channel.channel_type, current_url, 
                        msg="channel_type [{}] NOT IN current_url: {}".format(production_channel.channel_type, current_url))
        # Check the first step is there
        production_channel.wait_for_record_loaded()

        # Going back to the activated channel. Check its data
        beta_channel = production_channel.switch_tabs(target_po='edit')
        active_tab = beta_channel.get_active_tab()
        self.assertIn(beta_channel.channel_type, active_tab.get_attribute('href'),
                        msg="Current tab is not {}".format(beta_channel.channel_type.upper()))

        self.assertEquals(self.app_name, beta_channel.app_name.text, 
                            msg='After moving between channels, the APP_NAME for the first one has changed')
        self.assertEquals(self.app_descr, beta_channel.app_description.text, 
                            msg='After moving between channels, the APP_DESCRIPTION for the first one has changed')
        self.assertEquals(self.deploy_url, beta_channel.deploy_url.text,
                            msg='After moving between channels, the DEPLOY_URL for the first one has changed')

        thumb_location = beta_channel.get_thumbnail_location()
        thumb_base64 = self.helpers.remote_icon_to_base64(thumb_location)
        original_preview = self.helpers.icon_to_base64(self.icon)
        self.assertIn(original_preview, thumb_base64, 
                        msg="'After moving between channels, the ICON IMAGE for the first one has changed'")
        beta_channel.go_back()

    @jira('OWDAPPMAKE-483')
    def test_update_move_channels_two_channels(self):
        self._activate_channel(self.deploy_url)
        self.channel_type = 'production'
        self.icon = 'icon_2.png'
        self._activate_channel()
        self.channel_type = 'beta'
        self.icon = 'icon_1.png'

        beta_channel = self.project.go_to_channel(self.channel_type)
        production_channel = beta_channel.switch_tabs(target_po='edit')
        active_tab = production_channel.get_active_tab()
        self.assertIn(production_channel.channel_type, active_tab.get_attribute('href'), 
                        msg="Current tab is not {}".format(production_channel.channel_type.upper()))
        production_channel.wait_for_record_loaded()

        beta_channel = production_channel.switch_tabs(target_po='edit')
        active_tab = beta_channel.get_active_tab()
        self.assertIn(beta_channel.channel_type, active_tab.get_attribute('href'),
                        msg="Current tab is not {}".format(beta_channel.channel_type.upper()))

        self.assertEquals(self.app_name, beta_channel.app_name.text, 
                            msg='After moving between channels, the APP_NAME for the first one has changed')
        self.assertEquals(self.app_descr, beta_channel.app_description.text, 
                            msg='After moving between channels, the APP_DESCRIPTION for the first one has changed')
        self.assertEquals(self.deploy_url, beta_channel.deploy_url.text,
                            msg='After moving between channels, the DEPLOY_URL for the first one has changed')

        thumb_location = beta_channel.get_thumbnail_location()
        thumb_base64 = self.helpers.remote_icon_to_base64(thumb_location)
        original_preview = self.helpers.icon_to_base64(self.icon)
        self.assertIn(original_preview, thumb_base64, 
                        msg="'After moving between channels, the ICON IMAGE for the first one has changed'")

        beta_channel.go_back()

    def tearDown(self):
        self.project.delete_project()
        super(self.__class__, self).tearDown()