# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TESTNAME: Verify the Project name field
# DATASET
#   Scenario 1: Long name
#   Scenario 2: Name with capital and lower letters, numbers and spaces
#   Scenario 3: Name with strange characters
#   Scenario 4: All previous scenarios together
# PROCEDURE
# 1. Open Appmaker Client
# 2. Click on start now
# 3. Click on Project Name field and write something [choose scenario]
# EXPECTED RESULT
# The name of the project should be shown fine

import os
import time
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from seleniumtid.jira import jira


class OWDAppMake324(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()
        self.names = ['ThisisaverylongnameforaprojectLonglonglong',
                      '1234NameWithCapitalLetters and spaces 1234', u'StrangéChäràçtèrs@',
                      u'ThisisaverylongnameforaprojectWithStrangéChärà,çtèrs@1234']
        self.new_project_locator = ('xpath', u'//li[@id="{}"]')

    @jira("OWDAPPMAKE-324")
    def test_verify_project_name_field(self):
        main_page = StartPageObject()
        main_page.open()
        self.projects = main_page.view_projects()
        map(self._do_the_test, self.names)

    def _do_the_test(self, project_name):
        project = self.projects.create_new_project(project_name)
        project.wait_for_project_created()

        time.sleep(2)
        project.delete_project()
