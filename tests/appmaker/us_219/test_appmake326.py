# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME: Create more than one project for a user
# PROCEDURE
# 1. Open Appmaker Client
# 2. Click on start now (ER1)
# 3. Click on Project Name field and write something (ER2)
# 4. Tap on Create (ER3)
# 5. Tap on New Project again and create another project (ER4)
# EXPECTED RESULT
# ER1. User is taken to new screen with New Poject chart available
# ER2. Project chart is active when going on it and when typing something into Project Name field
# ER3. User is taken back to project list view. The new project is created with the name typed by user.
# ER4. A second project is successfully created the first in the project list. The project list view is shown fine.

import os
import uuid
import time
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from seleniumtid.jira import jira


class OWDAppMake326(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

    @jira("OWDAPPMAKE-326")
    def test_create_more_than_a_project(self):
        main_page = StartPageObject()
        main_page.open()
        projects = main_page.view_projects()

        project_name_1 = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project_1 = projects.create_new_project(project_name_1)
        self.project_1.wait_for_project_created()

        project_name_2 = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project_2 = projects.create_new_project(project_name_2)
        self.project_2.wait_for_project_created()

        # Check that the first one is still there after creating another project
        self.project_1.wait_for_project_created()

    def tearDown(self):
        self.project_1.delete_project()
        self.project_2.delete_project()
        super(self.__class__, self).tearDown()
