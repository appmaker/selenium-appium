# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME: Remove a production/beta channel

# PRERREQUISITES
# A channel should be activated for a project

# DATASET
#   Scenario 1: production channel
#   Scenario 2: beta channel

# PROCEDURE
#   1. Tap on an activated channel [Choose scenario]
#   2. Remove it


# EXPECTED RESULT
#   The channel is successfully 'deactivated' and is shown that way in project list view

import os
import time
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from seleniumtid.jira import jira
from utils.helpers import Helpers

class OWDAppMake343(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        # App info
        self.channel_type = 'production'
        self.app_name = "Test app name"
        self.app_descr = "This is a description for this app"
        self.icon = "icon_1.png"
        self.zip = "Here.zip"
        self.deploy_url = "test_url_{}".format(str(uuid.uuid4())[-5:])

        self.helpers = Helpers(self)

        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        self.projects = main_page.view_projects()

        # 2. Create a project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = self.projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

    def _activate_channel(self):
        # 3. Activate a channel (i.e. PRODUCTION)
        channel = self.project.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        channel.fill_zip(self.zip)
        channel.click_upload_zip_next_button()

        channel.fill_manifest(self.app_name, self.app_descr, self.icon)
        channel.click_app_manifest_next_button()

        image_preview = channel.preview_icon.element().get_attribute('style').split("data:image/png;base64,")[-1]
        utils_preview = self.helpers.icon_to_base64(self.icon)
        self.assertIn(utils_preview, image_preview, msg="Base64 of uploaded image does NOT match the original base64")

        channel.fill_deployment(self.deploy_url)
        self.project = channel.click_save_channel()
        self.project.wait_for_channel_created(self.channel_type)

    @jira('OWDAPPMAKE-343')
    def test_remove_production_beta_channel(self):
        self._activate_channel()
        channel = self.project.go_to_channel(self.channel_type)
        self.project = channel.delete_channel()
        self.project.wait_for_channel_deleted(self.channel_type)

        self.channel_type = 'beta'
        self._activate_channel()
        channel = self.project.go_to_channel(self.channel_type)
        self.project = channel.delete_channel()
        self.project.wait_for_channel_deleted(self.channel_type)

    def tearDown(self):
        self.project.delete_project()
        super(self.__class__, self).tearDown()