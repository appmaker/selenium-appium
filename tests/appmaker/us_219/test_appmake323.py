# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# OWDAPPMAKE-322 Verify the default label given
# PROCEDURE
#   1. Open Appmaker Client
#   2. Click on start now (ER1)
#   3. Go over New Project chart and check the default name given as an example (ER2)
# EXPECTED RESULT
#   ER1. User is taken to new screen with New Project chart available
#   ER2. Project chart is active when going on. Into Project Name field, it
#   should appear a name grayed out with a name as an example

import os
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from seleniumtid.jira import jira


class OWDAppMake323(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()
        # self.expected_default_value = 'Nombre del Proyecto...'
        self.expected_default_value = 'Project Name...'

    @jira('OWDAPPMAKE-323')
    def test_verify_default_label(self):
        main_page = StartPageObject()
        main_page.open()
        projects = main_page.view_projects()
        self.assertEquals(projects.project_name_input.element().get_attribute('placeholder'), self.expected_default_value)
