# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME: Delete a project - Cancel

# PROCEDURE
# 1. Go to project list view
# 2. Tap on the arrow on the right up corner of a chart (ER1)
# 3. Tap on Delete project (ER2)
# 4. Tap on Cancel (ER3)

# EXPECTED RESULT
# ER1. A menu with an option to Delete the project is shown
# ER2. A new window is open to confirm whether the user wants to delete the project
# ER3. The project is not deleted. User is taken back to project list view where the project appears as before

import os
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from seleniumtid.jira import jira


class OWDAppMake401(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

    @jira("OWDAPPMAKE-401")
    def test_delete_project_cancel(self):
        main_page = StartPageObject()
        main_page.open()
        projects = main_page.view_projects()

        prev_length = len(projects.project_cards())
        self.logger.info("prev_length: {}".format(prev_length))

        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        self.project.delete_project(cancel=True)

        current_length = len(projects.project_cards())
        self.logger.info("current_length: {}".format(current_length))
        self.assertEquals(current_length, prev_length + 1)

    def tearDown(self):
        self.project.delete_project()
        super(self.__class__, self).tearDown()
