# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# Rename a project - Close the project name window without doing changes

# PROCEDURE
# 1. Go to project list view
# 2. Tap on the arrow on the right up corner of a chart (ER1)
# 3. Tap on Project name to rename it (ER2)
# 4. Tap on the name and modify it (ER3)
# 5. Tap on Close button on the upper right corner (ER4)

# EXPECTED RESULT
# ER1. A menu with an option to rename the project is shown
# ER2. A new window is open, the current name of the project is shown
# ER3. The name can be changed
# ER4. The user is taken back to project list view and the name of the project has NOT been changed


import os
import time
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from selenium.webdriver.common.by import By
from seleniumtid.jira import jira


class OWDAppMake405(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        main_page = StartPageObject()
        main_page.open()
        projects = main_page.view_projects()

        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = projects.create_new_project(project_name)
        self.project.wait_for_project_created()

    @jira("OWDAPPMAKE-405")
    def test_edit_project_name_close(self):
        new_project_name = "{}_{}".format("new", self.project.project_name)
        self.project.select_edit()
        self.project.project_edit_input.element().clear()
        self.project.project_edit_input.text = new_project_name
        time.sleep(2)
        self.project.project_edit_back.element().click()
        time.sleep(2)

        self.assertNotEquals(self.project.project_card.element().get_attribute("data-project-name"), new_project_name)
        self.assertEquals(self.project.project_card.element().get_attribute("data-project-name"), self.project.project_name)

        self.assertNotEquals(self.project.project_card.element().find_element(
            By.CSS_SELECTOR, "h4").text, new_project_name)
        self.assertEquals(self.project.project_card.element().find_element(
            By.CSS_SELECTOR, "h4").text, self.project.project_name)

    def tearDown(self):
        self.project.delete_project()
        super(self.__class__, self).tearDown()
