# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME: Verify that the icon for each channel is correct

# PROCEDURE
#   1. On project list view, select one project
#   2. Tap on ACTIVATE a production channel (ER1)
#   3. Follow the Step 1, 2 and 3 to create the channel with the correct data required, select an icon, and tap on Save (ER2)
#   4. Do the same to ACTIVATE the same project's beta channel, selecting a different icon when required (ER3)

# EXPECTED RESULT
#   ER1. A new window is open to start the process to upload a file, complete the manifest and deploy
#   ER2. When the three steps are completed, user is taken back to project list view. There user can see the project's
#        channel activated with the correct info given. The icon chosen is shown ok
#   ER3. The channel appears correctly activated with its corresponding icon

import os
import time
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from seleniumtid.jira import jira
from utils.helpers import Helpers


class OWDAppMake335(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        # App info
        self.channel_type = 'beta'
        self.app_name = "Test app name for the {} channel".format(self.channel_type)
        self.app_descr = "This is a description for this {} app".format(self.channel_type)
        self.icon = "icon_1.png"
        self.zip = "Here.zip"

        self.helpers = Helpers(self)

    @jira('OWDAPPMAKE-335')
    def test_icon_for_each_channel(self):
        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        projects = main_page.view_projects()

        # 2. Create a project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

        # 3. Activate a channel (i.e. BETA)
        self._activate_channel()

        # Activate the other channel
        self.channel_type = 'production'
        self.app_name = "Test app name for the {} channel".format(self.channel_type)
        self.app_descr = "This is a description for this {} app".format(self.channel_type)
        self.icon = "icon_2.png"
        self._activate_channel()

    def _activate_channel(self):
        channel = self.project.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        channel.fill_zip(self.zip)
        channel.click_upload_zip_next_button()

        channel.fill_manifest(self.app_name, self.app_descr, self.icon)
        channel.click_app_manifest_next_button()

        image_preview = channel.preview_icon.element().get_attribute('style').split("data:image/png;base64,")[-1]
        utils_preview = self.helpers.icon_to_base64(self.icon)
        self.assertIn(utils_preview, image_preview, msg="Base64 of uploaded image does NOT match the original base64")

        channel.fill_deployment()
        self.project = channel.click_save_channel()
        self.project.wait_for_channel_created(self.channel_type)

        thumb_location = self.project.get_thumbnail_location(self.channel_type)
        print "THUMB LOCATION for {}: {}".format(self.channel_type, thumb_location)
        thumb_base64 = self.helpers.remote_icon_to_base64(thumb_location)
        self.assertEquals(utils_preview, thumb_base64, msg="Base64 of thumbnail does NOT match the original base64")

    def tearDown(self):
        self.project.delete_project()
        super(self.__class__, self).tearDown()
