# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME:  Move from one channel to the other when creating a channel

# PRERREQUISITES: There should be a project already created

# DATASET
#   Scenario 1: None of the channels activated
#   Scenario 2: One channel activated
#   Scenario 3: Both channels activated

# PROCEDURE
#   [Choose scenario]
#   Step only for scenario 1, tap on a channel to start the process to activate it.
#   1. On the left side of the screen, under CHANNELS label, tap on the other channel (ER1)
#   2. Then tap again on the previous channel (ER2)


# EXPECTED RESULT
#   ER1. User is taken to the other channel without errors
#   ER2. User is taken back to the channel he was previously

import os
import time
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from seleniumtid.jira import jira
from utils.helpers import Helpers
from seleniumtid import selenium_driver


class OWDAppMake342(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        # App info
        self.channel_type = 'beta'
        self.app_name = "Test app name"
        self.app_descr = "This is a description for this app"
        self.icon = "icon_1.png"
        self.zip = "Here.zip"
        self.helpers = Helpers(self)

        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        self.projects = main_page.view_projects()

        # 2. Create a project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = self.projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

    def _activate_channel(self):
        channel = self.project.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        channel.fill_zip(self.zip)
        channel.click_upload_zip_next_button()

        channel.fill_manifest(self.app_name, self.app_descr, self.icon)
        channel.click_app_manifest_next_button()

        image_preview = channel.preview_icon.element().get_attribute('style').split("data:image/png;base64,")[-1]
        utils_preview = self.helpers.icon_to_base64(self.icon)
        self.assertIn(utils_preview, image_preview, msg="Base64 of uploaded image does NOT match the original base64")

        channel.fill_deployment()
        self.project = channel.click_save_channel()
        self.project.wait_for_channel_created(self.channel_type)

    @jira('OWDAPPMAKE-342')
    def test_move_channels_no_channels(self):
        beta_channel = self.project.activate_channel(self.channel_type)
        production_channel = beta_channel.switch_tabs(target_po='create')

        current_url = self.driver.execute_script("""return window.location.href;""")
        active_tab = production_channel.get_active_tab()
        self.assertIn(production_channel.channel_type, current_url, msg="channel_type [{}] NOT IN current_url")
        self.assertIn(production_channel.channel_type, active_tab.get_attribute('href'), 
                        msg="Current tab is not {}".format(production_channel.channel_type.upper()))

        beta_channel = production_channel.switch_tabs(target_po='create')
        current_url = self.driver.execute_script("""return window.location.href;""")
        active_tab = beta_channel.get_active_tab()
        self.assertIn(beta_channel.channel_type, current_url, msg="channel_type [{}] NOT IN current_url")
        self.assertIn(beta_channel.channel_type, active_tab.get_attribute('href'), 
                        msg="Current tab is not {}".format(beta_channel.channel_type.upper()))
        beta_channel.go_back()

    @jira('OWDAPPMAKE-342')
    def test_move_channels_one_channel(self):
        self._activate_channel()

        beta_channel = self.project.go_to_channel(self.channel_type)
        production_channel = beta_channel.switch_tabs(target_po='create')

        current_url = self.driver.execute_script("""return window.location.href;""")
        active_tab = production_channel.get_active_tab()
        self.assertIn(production_channel.channel_type, active_tab.get_attribute('href'), 
                        msg="Current tab is not {}".format(production_channel.channel_type.upper()))
        self.assertIn(production_channel.channel_type, current_url, 
                        msg="channel_type [{}] NOT IN current_url: {}".format(production_channel.channel_type, current_url))

        # Going back to the activated channel.
        beta_channel = production_channel.switch_tabs(target_po='edit')
        active_tab = beta_channel.get_active_tab()
        self.assertIn(beta_channel.channel_type, active_tab.get_attribute('href'),
                        msg="Current tab is not {}".format(beta_channel.channel_type.upper()))
        beta_channel.go_back()

    @jira('OWDAPPMAKE-342')
    def test_move_channels_two_channels(self):
        self._activate_channel()
        self.channel_type = 'production'
        self._activate_channel()
        self.channel_type = 'beta'

        beta_channel = self.project.go_to_channel(self.channel_type)
        production_channel = beta_channel.switch_tabs(target_po='edit')
        active_tab = production_channel.get_active_tab()
        self.assertIn(production_channel.channel_type, active_tab.get_attribute('href'), 
                        msg="Current tab is not {}".format(production_channel.channel_type.upper()))

        beta_channel = production_channel.switch_tabs(target_po='edit')
        active_tab = beta_channel.get_active_tab()
        self.assertIn(beta_channel.channel_type, active_tab.get_attribute('href'),
                        msg="Current tab is not {}".format(beta_channel.channel_type.upper()))
        beta_channel.go_back()

    def tearDown(self):
        self.project.delete_project()
        super(self.__class__, self).tearDown()