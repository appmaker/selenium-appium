# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME: Verify that the settings button has the right options

# PROCEDURE
# 1. Go to project list view
# 2. Tap on the arrow on the right up corner of a chart (ER1)
# 3. Verify that the menu with the correct options is shown (ER2)

# EXPECTED RESULT
# ER1. A menu with an option to rename the project is shown
# ER2. At the moment the options are Project name and Delete project


import os
import time
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from seleniumtid.jira import jira


class OWDAppMake330(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()
        main_page = StartPageObject()
        main_page.open()
        projects = main_page.view_projects()

        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = projects.create_new_project(project_name)
        self.project.wait_for_project_created()

    @jira("OWDAPPMAKE-330")
    def test_verify_settings(self):
        self.project.open_options()
        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(self.project.project_options_edit.locator))
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(self.project.project_options_delete.locator))
        self.project.project_options_toggle.element().click()
        time.sleep(1)

    def tearDown(self):
        self.project.delete_project()
        super(self.__class__, self).tearDown()
