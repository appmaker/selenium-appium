# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME:  Verify that after deleting an app (project), the subdomain is free again

# PRERREQUISITES: App created with a valid deployment url

# DATASET
# Scenario 1: Project with only one channel active
# Scenario 2: Project with both channels active

# PROCEDURE
#   [Choose scenario]
#   -Delete project
#   -Create a new project and activate a channel with a valid app
#   -For the deployment url select the url of the app deleted when deleting the project


# EXPECTED RESULT
#   The new app can use the deployment url of the app deleted previously

import os
import time
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from seleniumtid.jira import jira
from utils.helpers import Helpers
from seleniumtid import selenium_driver


class OWDAppMake636(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        # App info
        self.channel_type = 'beta'
        self.app_name = "Test app name"
        self.app_descr = "This is a description for this app"
        self.icon = "icon_1.png"
        self.zip = "Here.zip"
        self.deploy_url = "test_deployment_{}".format(str(uuid.uuid4())[-6:])
        self.helpers = Helpers(self)

        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        self.projects = main_page.view_projects()

        # 2. Create a project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = self.projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

        # 3. Activate a channel (i.e. BETA)
        self._activate_channel()

    def _activate_channel(self, deploy_url=None):
        deploy_url = deploy_url if deploy_url is not None else self.deploy_url

        self.channel = self.project.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        self.channel.fill_zip(self.zip)
        self.channel.click_upload_zip_next_button()

        self.channel.fill_manifest(self.app_name, self.app_descr, self.icon)
        self.channel.click_app_manifest_next_button()

        image_preview = self.channel.preview_icon.element().get_attribute('style').split("data:image/png;base64,")[-1]
        utils_preview = self.helpers.icon_to_base64(self.icon)
        self.assertIn(utils_preview, image_preview, msg="Base64 of uploaded image does NOT match the original base64")

        self.channel.fill_deployment(deploy_url)
        self.project = self.channel.click_save_channel()
        self.project.wait_for_channel_created(self.channel_type)

    @jira('OWDAPPMAKE-636')
    def test_subdomain_free_one_channel(self):
        # 1. Now, delete the project
        self.project.delete_project()

        # 2. Create a project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = self.projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

        # 3. Create a channel, with the deployment url used before
        self.channel_type = 'beta'
        self._activate_channel()

    @jira('OWDAPPMAKE-636')
    def test_subdomain_free_two_channels(self):
        # 1. Activate the other channel
        self.channel_type = 'production'
        self.icon = "icon_2.png"
        self._activate_channel("another_test_deployment_{}".format(str(uuid.uuid4())[-6:]))

        # 2. Now, delete the project
        self.project.delete_project()

        # 2. Create a project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = self.projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

        # 3. Create a channel, with the deployment url used before
        self.channel_type = 'beta'
        self._activate_channel()

    def tearDown(self):
        self.project.delete_project()
        super(self.__class__, self).tearDown()