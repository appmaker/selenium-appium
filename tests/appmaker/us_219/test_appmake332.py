# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME: Verify the project's channel
# PREREQUESITES
#   There should be at least one project without channels activated
# PROCEDURE
#   1. 1. Go to project list view
#   2. Check the project created
# EXPECTED RESULT
#   The name of the project's channels and how they are presented (not activated by default) is correct

import os
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from selenium.webdriver.common.by import By
from seleniumtid.jira import jira


class OWDAppMake332(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()
        self.allowed_types = ['beta', 'production']

        main_page = StartPageObject()
        main_page.open()
        self.projects = main_page.view_projects()

        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = self.projects.create_new_project(project_name)
        self.project.wait_for_project_created()

    @jira("OWDAPPMAKE-332")
    def test_verify_projects_name_channel(self):
        self.logger.info("Checking for project: {}".format(self.project.project_name))

        channels = self.project.project_card.element().find_elements(By.CSS_SELECTOR, 'a.qa-channel-new')

        self.logger.info("Len of channels: %d" % len(channels)) 
        self.logger.info("Len of allowed types: %d" % len(self.allowed_types)) 
        self.assertEquals(len(channels), len(self.allowed_types), msg='There are NOT 2 channels for the project')

        production, beta = None, None
        production = self.project.activate_production_channel.element()
        beta = self.project.activate_beta_channel.element()

        self.assertIsNotNone(production,
                             msg='Project [] has the PRODUCTION option activated'.format(self.project.project_name))
        self.assertIsNotNone(beta,
                        msg='Project [] has the BETA option activated'.format(self.project.project_name))

    def tearDown(self):
        self.project.delete_project()
        super(self.__class__, self).tearDown()
