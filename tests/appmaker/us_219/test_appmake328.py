# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME: Verify the project list view
# PREREQUESITES
#   There should be several projects already created
# PROCEDURE
#   1. Enter in project list view when there are already several pojects created
# EXPECTED RESULT
#   The projects created are shown fine with their channels, icons and so

import os
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from seleniumtid.jira import jira


class OWDAppMake328(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        main_page = StartPageObject()
        main_page.open()
        self.projects = main_page.view_projects()

        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = self.projects.create_new_project(project_name)
        self.project.wait_for_project_created()

    @jira("OWDAPPMAKE-328")
    def test_verify_list_view(self):
        cards = self.projects.project_cards()
        self.assertGreaterEqual(len(cards), 1, msg='The number of projects is less than 1')
        map(self._check_card, cards)

    def _check_card(self, card):
        self.logger.info("Checking for card: {}".format(card.project_name))
        self.assertIsNotNone(card.project_options_toggle.element(),
                             msg='Project [] has no options toggle'.format(card.project_name))

        production, beta = None, None
        try:
            production = card.activate_production_channel.element()
        except:
            production = card.go_production_channel.element()

        try:
            beta = card.activate_beta_channel.element()
        except:
            beta = card.go_beta_channel.element()

        self.assertIsNotNone(production,
                             msg='Project [] has no production channel options'.format(card.project_name))
        self.assertTrue(beta,
                        msg='Project [] has no beta channel options'.format(card.project_name))

    def tearDown(self):
        self.project.delete_project()
        super(self.__class__, self).tearDown()
