# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME: Verify that it is possible to change all metadata of the app at once

# PRERREQUISITES
#   There is already a project created in appmaker with at least one channel successfully activated

# PROCEDURE
#   1-Go to project list view
#   2-Go to a project which already has a channel activated
#   3-Tap on an activated channel
#   4-Modify the name, the description and the icon of the app
#   5-Tap on Save


# EXPECTED RESULT
#   The name, the description and the icon of the app have been correctly updated

import os
import time
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from seleniumtid.jira import jira
from utils.helpers import Helpers


class OWDAppMake701(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        # App info
        self.channel_type = 'production'
        self.app_name = "Test app name"
        self.app_descr = "This is a description for this app"
        self.icon = "icon_1.png"
        self.zip = "Here.zip"

        self.helpers = Helpers(self)

        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        self.projects = main_page.view_projects()

        # 2. Create a project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = self.projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

        # 3. Activate a channel (i.e. PRODUCTION)
        channel = self.project.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        channel.fill_zip(self.zip)
        channel.click_upload_zip_next_button()

        channel.fill_manifest(self.app_name, self.app_descr, self.icon)
        channel.click_app_manifest_next_button()

        image_preview = channel.preview_icon.element().get_attribute('style').split("data:image/png;base64,")[-1]
        utils_preview = self.helpers.icon_to_base64(self.icon)
        self.assertIn(utils_preview, image_preview, msg="Base64 of uploaded image does NOT match the original base64")

        channel.fill_deployment()
        self.project = channel.click_save_channel()
        self.project.wait_for_channel_created(self.channel_type)

    @jira('OWDAPPMAKE-701')
    def test_edit_a_production_channel(self):
        new_app_name = '{} {}'.format('NEW', self.app_name)
        new_app_descr = '{} {}'.format('NEW', self.app_descr)
        new_icon = 'icon_2.png'

        channel = self.project.go_to_channel(self.channel_type)
        time.sleep(3)
        channel.edit_manifest_data(new_app_name, new_app_descr, new_icon)
        self.project = channel.save_channel()

        channel = self.project.go_to_channel(self.channel_type)
        current_app_name = channel.get_app_name()
        current_app_descr = channel.get_app_description()

        self.assertEquals(new_app_name, current_app_name, msg="App names DO NOT MATCH")
        self.assertEquals(new_app_descr, current_app_descr, msg="App descriptions DO NOT MATCH")

        # Check new image
        time.sleep(5)
        image_preview_url = channel.get_thumbnail_location()
        image_preview_base64 = self.helpers.remote_icon_to_base64(image_preview_url)
        utils_preview = self.helpers.icon_to_base64(new_icon)
        self.assertEquals(image_preview_base64, utils_preview, msg="Base 64 of the files DO NOT MATCH")

        channel.go_back()

    def tearDown(self):
        self.project.delete_project()
        super(self.__class__, self).tearDown()
