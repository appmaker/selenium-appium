# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME: Rename a project which already has at least one app

# PRERREQUISITES
#   There should be at least one project already created with at least one channel activated

# PROCEDURE
#   1. Go to project list view
#   2. Tap on the arrow on the right up corner of a chart (ER1)
#   3. Tap on Project name to rename it (ER2)
#   4. Tap on the name and modify it (ER3)
#   5. Tap on Save (ER4)


# EXPECTED RESULT
#   ER1. A menu with an option to rename the project is shown
#   ER2. A new window is open, the current name of the project is shown
#   ER3. The name can be changed
#   ER4. The name of the project is changed successfully. 

import os
import time
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from selenium.webdriver.common.by import By
from seleniumtid.jira import jira
from utils.helpers import Helpers


class OWDAppMake698(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        # App info
        self.channel_type = 'production'
        self.app_name = "Test app name"
        self.app_descr = "This is a description for this app"
        self.icon = "icon_1.png"
        self.zip = "Here.zip"

        self.helpers = Helpers(self)

        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        self.projects = main_page.view_projects()

        # 2. Create a project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = self.projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

        # 3. Activate a channel (i.e. BETA)
        channel = self.project.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        channel.fill_zip(self.zip)
        channel.click_upload_zip_next_button()

        channel.fill_manifest(self.app_name, self.app_descr, self.icon)
        channel.click_app_manifest_next_button()

        image_preview = channel.preview_icon.element().get_attribute('style').split("data:image/png;base64,")[-1]
        utils_preview = self.helpers.icon_to_base64(self.icon)
        self.assertIn(utils_preview, image_preview, msg="Base64 of uploaded image does NOT match the original base64")

        channel.fill_deployment()
        self.project = channel.click_save_channel()
        self.project.wait_for_channel_created(self.channel_type)

    @jira('OWDAPPMAKE-698')
    def test_create_a_production_channel(self):
        new_project_name = "{}_{}".format("new", self.project.project_name)
        self.project.edit_project(new_project_name)

        self.assertEquals(self.project.project_card.element().get_attribute("data-project-name"), new_project_name)
        self.assertEquals(self.project.project_card.element().find_element(By.CSS_SELECTOR, "h4").text, new_project_name)
        
    def tearDown(self):
        self.project.delete_project()
        super(self.__class__, self).tearDown()
