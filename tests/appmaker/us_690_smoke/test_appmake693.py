# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME: Verify the functionality of the Save button

# SCENARIO
#   Scenario 1: All fields are correct
#   Scenario 2: Missing or incorrect fields

# PROCEDURE
#   - Create a new project and name it
#   - Activate a channel
#   - Select zip as the way to upload the app
#   - Tap on Next
#   - Name the app
#   - Add a description
#   - Upload an icon
#   - Tap on Next
#   - Select the way to deploy
#   - Tap on Create [Scenario]

# EXPECTED RESULT
#   It is possible to tap on Create button
#   1. The app is created
#   2. The app is not created and errors are shown

import os
import time
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from seleniumtid.jira import jira
from utils.helpers import Helpers
from seleniumtid import selenium_driver


class OWDAppMake693(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        # App info
        self.channel_type = 'beta'
        self.app_name = "Test app name"
        self.app_descr = "This is a description for this app"
        self.icon = "icon_1.png"
        self.zip = "Here.zip"

        self.helpers = Helpers(self)
        self.here_locator = (By.CSS_SELECTOR, "div.mh5_brandLogo")
        self.wait_time = int(selenium_driver.config.get('Common', 'explicitly_wait'))
        # self.expected_deploy_error_msg = u"se debe completar,formato erróneo"
        self.expected_deploy_error_msg = u"can't be blank,wrong format"

    @jira('OWDAPPMAKE-693')
    def test_verify_functionality_save_button(self):
        self._test_data_ok()
        self._test_data_ko_step2()
        self._test_data_ko_step3()

    def _test_data_ko_step3(self):
        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        time.sleep(3)

        # We don't expect authentication error, since we have already been authenticated in the first test
        projects = main_page.view_projects(False)

        # 2. Create a project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

        # 3. Activate a channel (i.e. BETA)
        channel = self.project.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        channel.fill_zip(self.zip)
        channel.click_upload_zip_next_button()

        channel.fill_manifest(self.app_name, self.app_descr, self.icon)
        channel.click_app_manifest_next_button()

        image_preview = channel.preview_icon.element().get_attribute('style').split("data:image/png;base64,")[-1]
        utils_preview = self.helpers.icon_to_base64(self.icon)
        self.assertIn(utils_preview, image_preview, msg="Base64 of uploaded image does NOT match the original base64")

        channel.fill_deployment("")
        time.sleep(5)
        channel.save_button.element().click()
        WebDriverWait(self.driver, self.wait_time).until(EC.visibility_of_element_located(channel.deploy_url_error_msg.locator))
        error_msg = channel.deploy_url_error_msg.element().text
        self.assertEquals(self.expected_deploy_error_msg, error_msg, msg="Error messages DOES NOT match")

        channel.go_back()
        self.project.wait_for_project_created()
        self.project.delete_project()

    def _test_data_ko_step2(self):
        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        time.sleep(3)
        # We don't expect authentication error, since we have already been authenticated in the first test
        projects = main_page.view_projects(False)

        # 2. Create a project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

        # 3. Activate a channel (i.e. BETA) - WITHOUT FILLING MANIFEST DATA
        channel = self.project.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        channel.fill_zip(self.zip)
        channel.click_upload_zip_next_button()

        time.sleep(3)
        channel.app_name.element().clear()
        WebDriverWait(self.driver, self.wait_time).until(EC.visibility_of_element_located(channel.step2_next_button.locator))
        WebDriverWait(self.driver, self.wait_time).until(lambda s: 'disabled' in channel.step2_next_button.element().get_attribute('class'))

        channel.go_back()
        self.project.wait_for_project_created()
        self.project.delete_project()

    def _test_data_ok(self):
        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        projects = main_page.view_projects()

        # 2. Create a project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

        # 3. Activate a channel (i.e. BETA)
        channel = self.project.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        channel.fill_zip(self.zip)
        channel.click_upload_zip_next_button()

        channel.fill_manifest(self.app_name, self.app_descr, self.icon)
        channel.click_app_manifest_next_button()

        image_preview = channel.preview_icon.element().get_attribute('style').split("data:image/png;base64,")[-1]
        utils_preview = self.helpers.icon_to_base64(self.icon)
        self.assertIn(utils_preview, image_preview, msg="Base64 of uploaded image does NOT match the original base64")

        channel.fill_deployment()
        self.project = channel.click_save_channel()
        time.sleep(2)
        self.project.wait_for_channel_created(self.channel_type)

        # 4. Go to channel and open the deployed app
        channel = self.project.go_to_channel(self.channel_type)
        deploy = channel.get_deployment_url()
        self.logger.info("Deploy url: {}".format(deploy))

        handles = set(self.driver.window_handles)
        self.driver.execute_script('window.open("http://{}", "_blank");'.format(deploy))
        new_handles = set(self.driver.window_handles)
        app_tab = list(new_handles - handles)[0]
        self.driver.switch_to_window(app_tab)

        # 5. Check the app is properly hosted
        WebDriverWait(self.driver, self.wait_time).until(EC.visibility_of_element_located(self.here_locator))
        self.driver.close()
        time.sleep(2)
        self.driver.switch_to_window(list(handles.intersection(new_handles))[0])
        channel.go_back()
        self.project.wait_for_project_created()
        self.project.delete_project()

    def tearDown(self):
        super(self.__class__, self).tearDown()
