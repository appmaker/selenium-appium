# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME: Verify the length of the url

# PROCEDURE
# Create a project with very long name
# Upload a correct file
# Give a name and description for the app


# EXPECTED RESULT
# If the name of the project is so long that exceeds  the url lenght, there
# should be an error message

import os
import time
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from seleniumtid.jira import jira
from utils.helpers import Helpers
from seleniumtid import selenium_driver


class OWDAppMake1305(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        # App info
        self.channel_type = 'production'
        self.app_name = "Test app name"
        self.app_descr = "This is a description for this app"
        self.icon = "icon_1.png"
        self.zip = "VerySimple.zip"

        self.helpers = Helpers(self)
        self.expected_error_msg = u"is too long (maximum is 63 characters)"
        # self.expected_error_msg = u"es demasiado largo (como máximo 63 caracteres)"
        self.wait_time = int(selenium_driver.config.get('Common', 'explicitly_wait'))

    # @jira('OWDAPPMAKE-1305')
    def test_verify_url_length(self):
        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        projects = main_page.view_projects()

        # 2. Create project
        long_name = str(uuid.uuid4()) * 2
        project_name = "test_{}".format(long_name)
        self.project = projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

        # 3. Activate a channel (i.e. PRODUCTION)
        channel = self.project.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        channel.fill_zip(self.zip)
        channel.click_upload_zip_next_button()

        channel.fill_manifest(self.app_name, self.app_descr, self.icon)
        channel.click_app_manifest_next_button()

        image_preview = channel.preview_icon.element().get_attribute('style').split("data:image/png;base64,")[-1]
        utils_preview = self.helpers.icon_to_base64(self.icon)
        self.assertIn(utils_preview, image_preview, msg="Base64 of uploaded image does NOT match the original base64")

        channel.fill_deployment()

        # Check the error message
        WebDriverWait(self.driver, self.wait_time).until(EC.visibility_of_element_located(channel.deploy_url_error_msg.locator))
        self.assertEquals(self.expected_error_msg, channel.deploy_url_error_msg.element().text,
                      msg="expecting url too long")

        time.sleep(2)
        channel.go_back()

    def tearDown(self):
        self.project.delete_project()
        super(self.__class__, self).tearDown()
