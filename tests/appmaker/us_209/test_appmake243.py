# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME: Verify that the domain name is provided

# PRERREQUISITES
#   Create a project and activate one of the channels.
#   Upload a valid zip file and give the necessary info to get to Step 3 Deploy to

# PROCEDURE
#   1. Select to deply to: appmaker
#   2. Check whether the domain is given


# EXPECTED RESULT
#  The domain name should be provided when deploying the app

import os
import time
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from seleniumtid.jira import jira
from utils.helpers import Helpers


class OWDAppMake243(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        # App info
        self.channel_type = 'production'
        self.app_name = "Test app name"
        self.app_descr = "This is a description for this app"
        self.icon = "icon_1.png"
        self.zip = "VerySimple.zip"

        self.helpers = Helpers(self)

    @jira('OWDAPPMAKE-243')
    def test_domain_is_provided(self):
        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        self.projects = main_page.view_projects()

        # 2. Create a project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = self.projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

        # 3. Activate a channel (i.e. PRODUCTION)
        channel = self.project.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        channel.fill_zip(self.zip)
        channel.click_upload_zip_next_button()

        channel.fill_manifest(self.app_name, self.app_descr, self.icon)
        channel.click_app_manifest_next_button()

        image_preview = channel.preview_icon.element().get_attribute('style').split("data:image/png;base64,")[-1]
        utils_preview = self.helpers.icon_to_base64(self.icon)
        self.assertIn(utils_preview, image_preview, msg="Base64 of uploaded image does NOT match the original base64")

        # 4. Verify that the deployment url is provided

        subdomain = channel.deploy_url_copy_host.element().text
        domain = channel.deploy_url.text 

        default_deploy_url = domain + subdomain
        
        project_url = project_name + '.test-appmaker.tid.es'
        
        self.assertIn(default_deploy_url, project_url)
        
        self.project = channel.click_save_channel()

    def tearDown(self):
        self.project.wait_for_project_created()
        self.project.delete_project()
        super(self.__class__, self).tearDown()
