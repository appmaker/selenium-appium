# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
# Frist test TC 242
# Verify that the name of the subdomain is already provided

import os
import uuid
import time
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from seleniumtid.jira import jira

class OWDAppMake242(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        # Subdomain
        self.expected_default_value = '.test-appmaker.tid.es'
        
        # App info
        self.channel_type = 'production'
        self.app_name = "my app"
        self.app_descr = "my app description"
        self.zip_name = "VerySimple.zip"
        self.icon = "icon_1.png"
        
        # Open project list
    
        main_page = StartPageObject()
        main_page.open()
        self.projects = main_page.view_projects()

        # Create a project
        self.project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = self.projects.create_new_project(self.project_name)
        self.project.wait_for_project_created()
        time.sleep(2)
   

    #@jira('OWDAPPMAKE-242')
    def test_check_subdomain(self):
        # Create a channel
        channel = self.project.activate_channel(self.channel_type)
        
        channel.fill_zip(self.zip_name)
        channel.click_upload_zip_next_button()

        channel.fill_manifest(self.app_name, self.app_descr, self.icon)
        channel.click_app_manifest_next_button()

        subdomain = channel.deploy_url_copy_host.element().text
     
        self.assertEquals(subdomain, self.expected_default_value)
        
        self.project = channel.click_save_channel()

    def tearDown(self):
        
        self.project.wait_for_project_created()
        self.project.delete_project()
        super(self.__class__, self).tearDown()
