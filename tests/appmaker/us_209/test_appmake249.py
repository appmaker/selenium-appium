# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME: Verify the 'copy URL' functionality

# PROCEDURE
#  1. Select to deply to: appmaker
#  2. Type a deployment url
#  3. Click on the copy button
#  4. Paste the url in a valid place

# EXPECTED RESULT
#  The url is correctly copied and pasted.
#   Note: There is a warning to let user know how to do it:
#   Press Crlt + c to copy to clipboard, and the text is automatically selected

import os
import time
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from seleniumtid.jira import jira
from utils.helpers import Helpers
from seleniumtid import selenium_driver
from seleniumtid.pageelements import InputText, PageElement


class OWDAppMake249(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        # App info
        self.channel_type = 'production'
        self.app_name = "Test app name"
        self.app_descr = "This is a description for this app"
        self.icon = "icon_1.png"
        self.zip = "VerySimple.zip"

        self.helpers = Helpers(self)
        self.wait_time = int(selenium_driver.config.get('Common', 'explicitly_wait'))
        # self.deploy_url_copy_host_bt = PageElement(By.CSS_SELECTOR, 'span.text-to-copy b button')
        self.copy_msg = PageElement(By.CSS_SELECTOR, 'div.toast p')

    @jira('OWDAPPMAKE-249')
    def test_verify_copy_functionality(self):
        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        projects = main_page.view_projects()

        # 2. Create one project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

        # 3. Activate a channel (i.e. PRODUCTION)
        channel = self.project.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        channel.fill_zip(self.zip)
        channel.click_upload_zip_next_button()

        channel.fill_manifest(self.app_name, self.app_descr, self.icon)
        channel.click_app_manifest_next_button()

        image_preview = channel.preview_icon.element().get_attribute('style').split("data:image/png;base64,")[-1]
        utils_preview = self.helpers.icon_to_base64(self.icon)
        self.assertIn(utils_preview, image_preview, msg="Base64 of uploaded image does NOT match the original base64")

        channel.fill_deployment()

        # When clicking on Copy, we expect the pop up to appear
        channel.deploy_url_copy_button.element().click()

        WebDriverWait(self.driver, self.wait_time).until(EC.visibility_of_element_located(self.copy_msg.locator))

        self.assertIn(u"ctrl + c", self.copy_msg.element().text,
                      msg="expecting ctrl+c message")

        self.project = channel.click_save_channel()
        self.project.wait_for_channel_created(self.channel_type)

    def tearDown(self):
        self.project.wait_for_project_created()
        self.project.delete_project()
