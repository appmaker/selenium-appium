# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME: Deploy an app, do not type anything in 'Deployment url'

# PRERREQUISITES
#   Create a project and activate one of the channels.
#   Upload a valid zip file and give the necessary info to get to Step 3 Deploy to

# PROCEDURE
#   1. Select to deply to: appmaker
#   2. Do not type in deployment url
#   3. Tap on Save


# EXPECTED RESULT
#  The default url,with the name of the project as subdomain, is taken for the deployment. The app is deployed correctly

import os
import time
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from seleniumtid.jira import jira
from utils.helpers import Helpers
from seleniumtid import selenium_driver


class OWDAppMake404(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        # App info
        self.channel_type = 'production'
        self.app_name = "Test app name"
        self.app_descr = "This is a description for this app"
        self.icon = "icon_1.png"
        #self.zip = "simple-app-only-full.zip"
        self.zip = "Here.zip"

        self.helpers = Helpers(self)
        self.here_locator = (By.CSS_SELECTOR, "div.mh5_brandLogo")
        self.wait_time = int(selenium_driver.config.get('Common', 'explicitly_wait'))      

        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        self.projects = main_page.view_projects()

        # 2. Create a project
        self.project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = self.projects.create_new_project(self.project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

        # 3. Activate a channel (i.e. PRODUCTION)
        self.channel = self.project.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        self.channel.fill_zip(self.zip)
        self.channel.click_upload_zip_next_button()

        self.channel.fill_manifest(self.app_name, self.app_descr, self.icon)
        self.channel.click_app_manifest_next_button()

        image_preview = self.channel.preview_icon.element().get_attribute('style').split("data:image/png;base64,")[-1]
        utils_preview = self.helpers.icon_to_base64(self.icon)
        self.assertIn(utils_preview, image_preview, msg="Base64 of uploaded image does NOT match the original base64")

    @jira('OWDAPPMAKE-404')
    def test_deploy_default_url(self):
        # Verfiy that the default url is project_name+subdomain
        subdomain = self.channel.deploy_url_copy_host.element().text
        domain = self.channel.deploy_url.text 

        default_deploy_url = domain + subdomain
       
        project_url = self.project_name + '.test-appmaker.tid.es'
       
        self.assertEquals(default_deploy_url, project_url)
        
        # Tap on Save to create the channel
        self.project = self.channel.click_save_channel()
        self.project.wait_for_channel_created(self.channel_type)

        # 4. Go to channel and open the deployed app
        self.channel = self.project.go_to_channel(self.channel_type)
        deploy = self.channel.get_deployment_url()
        self.logger.info("Deploy url: {}".format(project_url))

        handles = set(self.driver.window_handles)
        self.driver.execute_script('window.open("http://{}", "_blank");'.format(project_url))
        new_handles = set(self.driver.window_handles)
        app_tab = list(new_handles - handles)[0]
        self.driver.switch_to_window(app_tab)

        # 5. Check the app is properly hosted
        WebDriverWait(self.driver, self.wait_time).until(EC.presence_of_element_located(self.here_locator))
        self.driver.close()
        time.sleep(2)
        self.driver.switch_to_window(list(handles.intersection(new_handles))[0])
        self.channel.go_back()
        
    def tearDown(self):
        self.project.wait_for_project_created()
        self.project.delete_project()
        super(self.__class__, self).tearDown()
