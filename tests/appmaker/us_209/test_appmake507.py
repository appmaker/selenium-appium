# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME: Verify that the deploy url is unique for each channel

# PROCEDURE
#   Create two projects with same name except for some letters or special chars
#   Activate a channel for each project going through step1, step3
#   On sept3 check the url for deploy

# EXPECTED RESULT
#   The url given for deploy should be unique for each channel

import os
import time
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from seleniumtid.jira import jira
from utils.helpers import Helpers
from seleniumtid import selenium_driver


class OWDAppMake507(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        # App info
        self.channel_type = 'production'
        self.app_name = "Test app name"
        self.app_descr = "This is a description for this app"
        self.icon = "icon_1.png"
        self.zip = "VerySimple.zip"

        self.helpers = Helpers(self)
        # self.expected_error_msg = u"Subdominio ya se ha utilizado"
        self.expected_error_msg = "Subdomain has already been taken"
        self.wait_time = int(selenium_driver.config.get('Common', 'explicitly_wait'))

    @jira('OWDAPPMAKE-507')
    def test_verify_unique_url(self):
        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        projects = main_page.view_projects()

        # 2. Create 1st project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project1 = projects.create_new_project(project_name)
        self.project1.wait_for_project_created()
        time.sleep(2)

        # 3. Activate a channel (i.e. PRODUCTION)
        channel = self.project1.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        channel.fill_zip(self.zip)
        channel.click_upload_zip_next_button()

        channel.fill_manifest(self.app_name, self.app_descr, self.icon)
        channel.click_app_manifest_next_button()

        image_preview = channel.preview_icon.element().get_attribute('style').split("data:image/png;base64,")[-1]
        utils_preview = self.helpers.icon_to_base64(self.icon)
        self.assertIn(utils_preview, image_preview, msg="Base64 of uploaded image does NOT match the original base64")

        channel.fill_deployment()
        self.project1 = channel.click_save_channel()
        self.project1.wait_for_channel_created(self.channel_type)

        # 2. Create 2nd project, like 1st project but capital letters
        project_name = project_name.upper()
        self.project2 = projects.create_new_project(project_name)
        self.project2.wait_for_project_created()
        time.sleep(2)

        # 3. Activate a channel (i.e. PRODUCTION)
        channel = self.project2.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        channel.fill_zip(self.zip)
        channel.click_upload_zip_next_button()

        channel.fill_manifest(self.app_name, self.app_descr, self.icon)
        channel.click_app_manifest_next_button()

        image_preview = channel.preview_icon.element().get_attribute('style').split("data:image/png;base64,")[-1]
        utils_preview = self.helpers.icon_to_base64(self.icon)
        self.assertIn(utils_preview, image_preview, msg="Base64 of uploaded image does NOT match the original base64")

        channel.fill_deployment()
        channel.click_save_channel(expected_error=True)

        # Checking that the error message is the expected
        deploy_error = channel.deploy_url_error_msg.element()
        self.assertEquals(self.expected_error_msg, deploy_error.text, msg="expecting Subdomain in use error message")

        channel.go_back()

        self.project2.delete_project()

    def tearDown(self):
        self.project1.wait_for_project_created()
        self.project1.delete_project()

        super(self.__class__, self).tearDown()
