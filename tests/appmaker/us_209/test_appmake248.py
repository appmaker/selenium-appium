# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

# TEST NAME: Verify the full hosting url

# PRERREQUISITES
#   Create a project and activate one of the channels.
#   Upload a valid zip file and give the necessary info to get to Step 3 Deploy to

# PROCEDURE
#   1. Select to deply to: appmaker
#   2. Type a deployment url
#   3. Check whether the url generated is right


# EXPECTED RESULT
#  The url generated should match the url typed by user
#  It should be a valid url accesible from a browser

import os
import time
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from seleniumtid.jira import jira
from utils.helpers import Helpers
from seleniumtid import selenium_driver

class OWDAppMake248(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()
    
        #  App info
        self.channel_type = 'production'
        self.app_name = "my app"
        self.app_descr = "my app description"
        self.zip_name = "Here.zip"
        self.icon = "icon_1.png"
        self.deployment_url = "My_url"
        self.expected_subdomain = ".test-appmaker.tid.es"
        
        self.helpers = Helpers(self)
        self.here_locator = (By.CSS_SELECTOR, "div.mh5_brandLogo")
        self.wait_time = int(selenium_driver.config.get('Common', 'explicitly_wait'))
        
    @jira('OWDAPPMAKE-248')
    def test_verify_full_hosting_url(self):
        #Open project list
        main_page = StartPageObject()
        main_page.open()
        self.projects = main_page.view_projects()

        #Create a project
        self.project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = self.projects.create_new_project(self.project_name)
        self.project.wait_for_project_created()
        time.sleep(2)
   
        #Activate a channel
        channel = self.project.activate_channel(self.channel_type)
        
        channel.fill_zip(self.zip_name)
        channel.click_upload_zip_next_button()

        channel.fill_manifest(self.app_name, self.app_descr, self.icon)
        channel.click_app_manifest_next_button()

        channel.fill_deployment(self.deployment_url)
        
        deploy_url = channel.deploy_url.text + channel.deploy_url_copy_host.element().text
        expected_deployment_url = self.deployment_url.lower() + self.expected_subdomain
     
        self.assertEquals(deploy_url, expected_deployment_url, msg="Deploy url NOT expected")

        self.project = channel.click_save_channel()
        
        self.project.wait_for_channel_created(self.channel_type)
        
        #Go to channel and open the deployed app
        channel = self.project.go_to_channel(self.channel_type)
        deploy = channel.get_deployment_url()
        self.logger.info("Deploy url: {}".format(deploy))

        handles = set(self.driver.window_handles)
        self.driver.execute_script('window.open("http://{}", "_blank");'.format(deploy))
        new_handles = set(self.driver.window_handles)
        app_tab = list(new_handles - handles)[0]
        self.driver.switch_to_window(app_tab)

        #Check the app is properly hosted
        WebDriverWait(self.driver, self.wait_time).until(EC.presence_of_element_located(self.here_locator))
        self.driver.close()
        time.sleep(2)
        self.driver.switch_to_window(list(handles.intersection(new_handles))[0])
        channel.go_back()

    def tearDown(self):
        self.project.wait_for_project_created()
        self.project.delete_project()
        super(self.__class__, self).tearDown()
