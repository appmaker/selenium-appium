# -*-# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
# TEST NAME: Verify that the sub-domain field cannot contain strange characters

# PROCEDURE
#  1. Select to deply to: appmaker
#  2. Type a deployment url writing strange characters

# EXPECTED RESULT
# Url should only contain valid characters

import os
import time
import uuid
from seleniumtid.test_cases import SeleniumTestCase
from page_objects.start_page import StartPageObject
from seleniumtid.jira import jira
from utils.helpers import Helpers
from seleniumtid import selenium_driver


class OWDAppMake252(SeleniumTestCase):

    def setUp(self):
        os.environ['Files_properties'] = 'conf/chrome-properties.cfg'
        super(self.__class__, self).setUp()

        #  App info
        self.channel_type = 'production'
        self.app_name = "my app"
        self.app_descr = "my app description"
        self.zip_name = "simple-app-only-full.zip"
        self.icon = "icon_1.png"
        self.deployment_url = "strang@_&"

        # self.expected_error_msg = u"formato erróneo"
        self.expected_error_msg = u"wrong format"
        self.helpers = Helpers(self)

    @jira('OWDAPPMAKE-252')
    def test_url_strange_chars(self):
        # 1. Open /projects
        main_page = StartPageObject()
        main_page.open()
        projects = main_page.view_projects()

        # 2. Create one project
        project_name = "test_{}".format(str(uuid.uuid4())[-10:])
        self.project = projects.create_new_project(project_name)
        self.project.wait_for_project_created()
        time.sleep(2)

        # 3. Activate a channel (i.e. PRODUCTION)
        channel = self.project.activate_channel(self.channel_type)
        # TODO - make sure zip method is selected
        channel.fill_zip(self.zip_name)
        channel.click_upload_zip_next_button()

        channel.fill_manifest(self.app_name, self.app_descr, self.icon)
        channel.click_app_manifest_next_button()

        image_preview = channel.preview_icon.element().get_attribute('style').split("data:image/png;base64,")[-1]
        utils_preview = self.helpers.icon_to_base64(self.icon)
        self.assertIn(utils_preview, image_preview, msg="Base64 of uploaded image does NOT match the original base64")

        channel.fill_deployment(self.deployment_url)

        channel.click_save_channel(expected_error=True)
        deploy_error = channel.deploy_url_error_msg.element()

        # Checking that the error message is the expected
        self.assertEquals(self.expected_error_msg, deploy_error.text, msg="expecting wrong format error")
        channel.go_back()

    def tearDown(self):
        self.project.wait_for_project_created()
        self.project.delete_project()
        super(self.__class__, self).tearDown()
