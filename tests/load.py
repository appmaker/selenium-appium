import os
import threading
import subprocess32
from time import sleep

class Load(object):
    def __init__(self):
        self.users = 2

    def run_load_smoke(self):
        cmd = 'nosetests -s {}'.format(os.path.join(os.getcwd(), 'tests', 'appmaker', 'smoke'))
        result = subprocess32.check_output(cmd, shell=True)

    def go(self):
        for i in range(self.users):
            th = threading.Thread(target=self.run_load_smoke)
            th.start()

if __name__ == '__main__':
    l = Load()
    l.go()