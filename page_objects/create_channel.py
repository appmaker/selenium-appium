# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
import os
import time
from seleniumtid.pageobjects.page_object import PageObject
from seleniumtid.pageelements import InputText, PageElement
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from seleniumtid import selenium_driver
from utils.helpers import scroll_into_view


class CreateChannelPageObject(PageObject):

    def __init__(self, project_name, channel_type):
        self.zip_path = os.path.join(os.getcwd(), "resources", "zips")
        self.icon_path = os.path.join(os.getcwd(), "resources", "icons")
        self.project_name = project_name
        self.channel_type = channel_type
        self.wait_time = int(selenium_driver.config.get('Common', 'explicitly_wait'))

        super(self.__class__, self).__init__()
        self.wait_for_record_loaded()

    def init_page_elements(self):

        # Header
        self.go_back_button = PageElement(By.CSS_SELECTOR, '#button-project-back')
        self.project_title = PageElement(
            By.XPATH, u'//header[contains(@class, "project-title")]//h2[text()="{}"]'.format(self.project_name))

        # Tabs (when one is active the other is enabled)
        self.production_tab = PageElement(By.ID, 'tab-channel-production')
        self.beta_tab = PageElement(By.ID, 'tab-channel-beta')
        self.active_tab = PageElement(By.CSS_SELECTOR, 'li.active a')

        # Accordion
        self.accordion = PageElement(By.CSS_SELECTOR, 'section.accordion.qa-channels-new')
        self.current_item_accordion = PageElement(By.CSS_SELECTOR, 'article.item-accordion[expand=true]:not(.positive)')

        # Step 1 - UPLOAD ZIP
        self.upload_zip_input = PageElement(By.CSS_SELECTOR, 'input.qa-channel-origin-zip')
        # Remember to check that this button should be disabled until the zip
        # began to upload (has css class 'disabled')
        self.step1_next_button = PageElement(By.CSS_SELECTOR, 'span.qa-channels-new-save-origin button:not(.disabled)')

        # Step 2 - MANIFEST
        self.app_name = InputText(By.CSS_SELECTOR, 'input.qa-channel-app-name')
        self.app_name_error_msg = PageElement(By.CSS_SELECTOR, 'input.qa-channel-app-name + + div.tc-message')
        self.app_description = InputText(By.CSS_SELECTOR, 'textarea.qa-channel-app-description')
        self.app_description_error_msg = PageElement(
            By.CSS_SELECTOR, 'textarea.qa-channel-app-description + div.tc-message')

        self.upload_icon_input = PageElement(By.CSS_SELECTOR, 'input.qa-channel-app-icon')
        self.upload_icon_error_msg = PageElement(By.XPATH, '//input[contains(@class, "qa-channel-app-icon")]/..//div[@class="tc-message"]')
        self.preview_icon = PageElement(By.CSS_SELECTOR, 'div.preview-image')
        self.step2_next_button = PageElement(By.CSS_SELECTOR, 'span.qa-channels-new-save-manifest button')

        # Step 3 - DEPLOYMENTS
        self.deploy_url = InputText(By.CSS_SELECTOR, 'input.qa-channel-deployment-subdomain')
        self.deploy_url_error_msg = PageElement(
            By.CSS_SELECTOR, 'input.qa-channel-deployment-subdomain + div.tc-message')
        self.deploy_url_copy = PageElement(By.CSS_SELECTOR, 'span.text-to-copy span')
        self.deploy_url_copy_host = PageElement(By.CSS_SELECTOR, 'span.text-to-copy b')
        self.deploy_url_copy_button = PageElement(By.CSS_SELECTOR, 'div[type=copy] button')

        # Buttons
        self.save_button = PageElement(By.CSS_SELECTOR, 'span.qa-channels-new-save button')

        # Saved channel dialog (@DEV - Add locators for qa)
        self.channel_saved_dialog = PageElement(By.CSS_SELECTOR, 'div.tc-dialog')
        self.channel_saved_dialog_ok_button = PageElement(By.CSS_SELECTOR, 'div.tc-dialog button.tc-button')

    def wait_for_record_loaded(self):
        WebDriverWait(self.driver, self.wait_time).until(EC.visibility_of_element_located(self.accordion.locator))
        WebDriverWait(self.driver, self.wait_time).until(EC.visibility_of_element_located(self.project_title.locator))
        WebDriverWait(self.driver, self.wait_time).until(EC.presence_of_element_located(self.upload_zip_input.locator))

    def switch_tabs(self, target_po):
        tab = self.production_tab if self.channel_type == 'beta' else self.beta_tab
        tab.element().click()

        target_po_channel_type = 'production' if self.channel_type == 'beta' else 'beta'
        if target_po == 'create':
            return  CreateChannelPageObject(self.project_name, target_po_channel_type)
        elif target_po == 'edit':
            from page_objects.edit_channel import EditChannelPageObject
            return EditChannelPageObject(self.project_name, target_po_channel_type)

    def get_active_tab(self):
        return self.active_tab.element()

    def fill_zip(self, zip_name):
        '''
        step 1
        '''
        self.upload_file(zip_name)

    def fill_manifest(self, app_name, app_description, icon=None):
        '''
        step 2
        '''
        self.app_name.element().clear()
        self.app_name.text = app_name

        self.app_description.element().clear()
        self.app_description.text = app_description

        if icon is not None:
            self.upload_icon(icon)

    def fill_deployment(self, deployment_url=None):
        '''
        step 3
        '''
        if deployment_url is not None:
            self.deploy_url.element().clear()
            self.deploy_url.text = deployment_url

    def click_upload_zip_next_button(self):
        self.step1_next_button.element().click()
        WebDriverWait(self.driver, self.wait_time * 2).until(EC.visibility_of_element_located(self.app_name.locator))
        WebDriverWait(self.driver, self.wait_time).until(
            EC.visibility_of_element_located(self.app_description.locator))
        WebDriverWait(self.driver, self.wait_time).until(
            EC.presence_of_element_located(self.upload_icon_input.locator))
        WebDriverWait(self.driver, self.wait_time).until(
            EC.visibility_of_element_located(self.step2_next_button.locator))

    def click_app_manifest_next_button(self):
        self.step2_next_button.element().click()
        WebDriverWait(self.driver, self.wait_time).until(EC.visibility_of_element_located(self.deploy_url.locator))
        WebDriverWait(self.driver, self.wait_time).until(EC.visibility_of_element_located(self.save_button.locator))

    def click_save_channel(self, expected_error=False):
        time.sleep(1)
        self.save_button.element().click()

        if expected_error:
            WebDriverWait(self.driver, self.wait_time).until(EC.visibility_of_element_located(self.deploy_url_error_msg.locator))
            return self

        WebDriverWait(self.driver, 30).until(EC.visibility_of_element_located(self.channel_saved_dialog.locator))
        WebDriverWait(self.driver, 30).until(EC.visibility_of_element_located(self.channel_saved_dialog_ok_button.locator))
        self.channel_saved_dialog_ok_button.element().click()
        self.go_back()

        from page_objects.project_card import ProjectCardPageObject
        return ProjectCardPageObject(self.project_name)

    def upload_file(self, file_name):
        self.upload_zip_input.element().send_keys(os.path.join(self.zip_path, file_name))

    def upload_icon(self, file_name):
        self.upload_icon_input.element().send_keys(os.path.join(self.icon_path, file_name))

    def go_back(self):
        scroll_into_view(self.driver, self.go_back_button.element())
        self.go_back_button.element().click()
