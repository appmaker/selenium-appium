# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
import re
import time
from seleniumtid.pageobjects.page_object import PageObject
from seleniumtid.pageelements import InputText, PageElement
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from page_objects.create_channel import CreateChannelPageObject
from page_objects.edit_channel import EditChannelPageObject


class ProjectCardPageObject(PageObject):

    def __init__(self, project_name):
        self.project_name = project_name
        self._allowed_channels = ['beta', 'production']
        super(self.__class__, self).__init__()

    def init_page_elements(self):
        # Locators
        self._project_root = u'//li[@data-project-name="{}"]//'.format(self.project_name)

        # Project card
        self.project_card = PageElement(By.XPATH, u'//li[@data-project-name="{}"]'.format(self.project_name))

        # Project card options
        self.project_options_toggle = PageElement(By.XPATH, self._project_root + 'span[contains(@class, "qa-bubble-menu-toggle")]')
        self.show_menu = PageElement(By.XPATH, self._project_root + 'i[contains(@class, "toggle-menu-icon")]')
        self.project_options_edit = PageElement(By.XPATH, self._project_root + 'li[contains(@class, "qa-bubble-menu-edit")]')
        self.project_options_delete = PageElement(By.XPATH, self._project_root + 'li[contains(@class, "qa-bubble-menu-delete")]')

        # Edit
        self.project_edit_section = PageElement(By.XPATH, '//section[@class="tc-modal show"]//div[@class="tc-dialog"]')
        self.project_edit_back = PageElement(By.XPATH, '//a[contains(@class,"qa-project-edit-back")]')
        self.project_edit_input = InputText(By.ID, 'editProjectNameField')
        self.project_edit_error_msg = PageElement(By.XPATH, '//input[@id="editProjectNameField"]/..//div[@class="tc-message"]')
        self.project_edit_save = PageElement(By.XPATH, '//button[contains(@class, "qa-project-edit-save")]')
        # TODO - Ask @Dev to add a locator for this element
        self.project_edit_cancel = PageElement(By.XPATH, '//button[@class="tc-button subdued"]')

        # Delete
        self.project_delete_section = PageElement(By.XPATH, self._project_root + 'div[contains(@class, "confirm-box")]')
        self.project_delete_confirm = PageElement(By.XPATH, self._project_root + 'button[contains(@class, "qa-project-delete-confirm")]')
        self.project_delete_cancel = PageElement(By.XPATH, self._project_root + 'button[contains(@class, "qa-project-delete-cancel")]')

        # Channels
        self.activate_production_channel = PageElement(By.XPATH, self._project_root +
                                                       'a[contains(@class, "qa-channel-new") and @data-tag="production"]')
        self.activate_beta_channel = PageElement(By.XPATH, self._project_root +
                                                 'a[contains(@class, "qa-channel-new") and @data-tag="beta"]')

        self.go_production_channel = PageElement(By.XPATH, self._project_root +
                                                       'a[contains(@class, "qa-channel-go") and @data-tag="production"]/div[@class="item-details"]')
        self.go_beta_channel = PageElement(By.XPATH, self._project_root +
                                                       'a[contains(@class, "qa-channel-go") and @data-tag="beta"]/div[@class="item-details"]')

        self.production_thumbnail = PageElement(By.XPATH, self._project_root +
                                                       'a[contains(@class, "qa-channel-go") and @data-tag="production"]//div[@class="project-icon"]')
        self.beta_thumbnail = PageElement(By.XPATH, self._project_root +
                                                       'a[contains(@class, "qa-channel-go") and @data-tag="beta"]//div[@class="project-icon"]')

    def get_thumbnail_location(self, channel_type):
        if channel_type not in self._allowed_channels:
            return
        thumbnail_element = self.beta_thumbnail if channel_type == 'beta' else self.production_thumbnail
        style = thumbnail_element.element().get_attribute('style')
        return re.search('url\((.*)\);', style).group(1)

    def wait_for_project_created(self):
        return WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(self.project_card.locator))

    def open_options(self):
        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(self.project_options_toggle.locator))
        self.project_options_toggle.element().click()
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located(self.show_menu.locator))

    def select_edit(self):
        self.open_options()
        self.project_options_edit.element().click()
        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(self.project_edit_section.locator))

    def select_delete(self):
        self.open_options()
        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(self.project_options_delete.locator))
        self.project_options_delete.element().click()
        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(self.project_delete_section.locator))

    def edit_project(self, new_name, cancel=False):
        self.select_edit()
        self.project_edit_input.element().clear()
        self.project_edit_input.text = new_name

        button = self.project_edit_cancel if cancel else self.project_edit_save
        button.element().click()

        time.sleep(3)

        # Since the name changes, the id changes, we have to call again init_page_elements
        if not cancel:
            self.project_name = new_name
            self.init_page_elements()

    def delete_project(self, cancel=False):
        time.sleep(2)
        self.select_delete()
        button = self.project_delete_cancel if cancel else self.project_delete_confirm
        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(button.locator))
        time.sleep(1)
        button.element().click()
        if not cancel:
            WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(self.project_card.locator))

    def activate_channel(self, channel):
        if channel not in self._allowed_channels:
            return
        button = self.activate_beta_channel if channel == 'beta' else self.activate_production_channel
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located(button.locator))
        button.element().click()
        return CreateChannelPageObject(self.project_name, channel)

    def go_to_channel(self, channel):
        if channel not in self._allowed_channels:
            return
        button = self.go_beta_channel if channel == 'beta' else self.go_production_channel
        time.sleep(3)
        button.element().click()
        return EditChannelPageObject(self.project_name, channel)

    def wait_for_channel_created(self, channel):
        '''
        This method ensures the existence of the link to a specific channel
        '''
        button = self.go_beta_channel if channel == 'beta' else self.go_production_channel
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located(button.locator))

    def wait_for_channel_deleted(self, channel):
        '''
        This method ensures the existance of the link to a specific channel
        '''
        button = self.activate_beta_channel if channel == 'beta' else self.activate_production_channel
        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(button.locator))
