# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
import time
import os
import re
from seleniumtid.pageobjects.page_object import PageObject
from seleniumtid.pageelements import InputText, PageElement
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from seleniumtid import selenium_driver
from utils.helpers import scroll_into_view


class EditChannelPageObject(PageObject):

    def __init__(self, project_name, channel_type):
        self.zip_path = os.path.join(os.getcwd(), "resources", "zips")
        self.icon_path = os.path.join(os.getcwd(), "resources", "icons")
        self.project_name = project_name
        self.channel_type = channel_type
        self.wait_time = int(selenium_driver.config.get('Common', 'explicitly_wait'))

        super(self.__class__, self).__init__()
        self.wait_for_record_loaded()

    def init_page_elements(self):
        # Header
        self.go_back_button = PageElement(By.CSS_SELECTOR, '#button-project-back')
        self.project_title = PageElement(
            By.XPATH, u'//header[contains(@class, "project-title")]//h2[text()="{}"]'.format(self.project_name))

        # Tabs (when one is active the other is enabled)
        self.production_tab = PageElement(By.ID, 'tab-channel-production')
        self.beta_tab = PageElement(By.ID, 'tab-channel-beta')
        self.active_tab = PageElement(By.CSS_SELECTOR, 'li.active a')

        # Record
        self.record = PageElement(By.CSS_SELECTOR, 'div.qa-channel')

        # Step 1 - UPLOAD ZIP
        self.upload_zip_input = PageElement(By.CSS_SELECTOR, 'input.qa-channel-origin-zip')

        # Step 2 - MANIFEST
        self.app_name = InputText(By.CSS_SELECTOR, 'input.qa-channel-app-name')
        self.app_description = InputText(By.CSS_SELECTOR, 'textarea.qa-channel-app-description')
        self.upload_icon_input = PageElement(By.CSS_SELECTOR, 'input.qa-channel-app-icon')
        self.preview_icon = PageElement(By.CSS_SELECTOR, 'div.preview-image')

        # Step 3 - DEPLOYMENTS
        self.deploy_url = InputText(By.CSS_SELECTOR, 'input.qa-channel-deployment-subdomain')
        self.deploy_url_copy = PageElement(By.CSS_SELECTOR, 'span.text-to-copy span')
        self.deploy_url_copy_host = PageElement(By.CSS_SELECTOR, 'span.text-to-copy b')

        # Buttons
        self.save_button = PageElement(By.CSS_SELECTOR, '#button-channel-save')
        self.delete_button = PageElement(By.CSS_SELECTOR, '#button-channel-delete')

        # Delete confirm
        self.delete_confirm_box = PageElement(By.CSS_SELECTOR, 'div.qa-channel div.confirm-box')
        self.delete_cancel = PageElement(By.CSS_SELECTOR, 'button.qa-channel-delete-cancel')
        self.delete_confirm = PageElement(By.CSS_SELECTOR, 'button.qa-channel-delete-confirm')

    def switch_tabs(self, target_po):
        tab = self.production_tab if self.channel_type == 'beta' else self.beta_tab
        tab.element().click()

        target_po_channel_type = 'production' if self.channel_type == 'beta' else 'beta'
        if target_po == 'create':
            from page_objects.create_channel import CreateChannelPageObject
            return  CreateChannelPageObject(self.project_name, target_po_channel_type)
        elif target_po == 'edit':
            return EditChannelPageObject(self.project_name, target_po_channel_type)

    def get_active_tab(self):
        return self.active_tab.element()

    def get_deployment_url(self):
        return self.deploy_url_copy.element().text + self.deploy_url_copy_host.element().text

    def get_app_name(self):
        return self.app_name.text

    def get_app_description(self):
        return self.app_description.text

    def get_thumbnail_location(self):
        style = self.preview_icon.element().get_attribute('style')
        return re.search('url\((.*)\);', style).group(1)

    def wait_for_record_loaded(self):
        WebDriverWait(self.driver, self.wait_time).until(EC.visibility_of_element_located(self.record.locator))
        WebDriverWait(self.driver, self.wait_time).until(EC.visibility_of_element_located(self.project_title.locator))
        WebDriverWait(self.driver, self.wait_time).until(EC.presence_of_element_located(self.upload_zip_input.locator))
        WebDriverWait(self.driver, self.wait_time).until(EC.visibility_of_element_located(self.save_button.locator))

    def save_channel(self):
        scroll_into_view(self.driver, self.save_button.element())
        time.sleep(5)
        self.save_button.element().click()
        self.go_back()
        WebDriverWait(self.driver, self.wait_time * 2).until(
            lambda s: s.current_url == selenium_driver.config.get('Common', 'url') + 'projects')

        from page_objects.project_card import ProjectCardPageObject
        return ProjectCardPageObject(self.project_name)

    def delete_channel(self, cancel=False):
        scroll_into_view(self.driver, self.delete_button.element())
        self.delete_button.element().click()
        WebDriverWait(self.driver, self.wait_time).until(
            EC.visibility_of_element_located(self.delete_confirm_box.locator))
        button = self.delete_cancel if cancel else self.delete_confirm
        time.sleep(1)
        button.element().click()

        if not cancel:
            WebDriverWait(self.driver, self.wait_time).until(
                lambda s: s.current_url == selenium_driver.config.get('Common', 'url') + 'projects')

            from page_objects.project_card import ProjectCardPageObject
            return ProjectCardPageObject(self.project_name)

    def edit_manifest_data(self, new_app_name=None, new_app_descr=None, new_icon=None):
        if new_app_name is not None:
            self.app_name.element().clear()
            self.app_name.text = new_app_name

        if new_app_descr is not None:
            self.app_description.element().clear()
            self.app_description.text = new_app_descr

        if new_icon is not None:
            self.upload_icon(new_icon)

    def go_back(self):
        scroll_into_view(self.driver, self.go_back_button.element())
        self.go_back_button.element().click()

    def upload_file(self, file_name):
        self.upload_zip_input.element().send_keys(os.path.join(self.zip_path, file_name))

    def upload_icon(self, file_name):
        self.upload_icon_input.element().send_keys(os.path.join(self.icon_path, file_name))
