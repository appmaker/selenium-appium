# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

from seleniumtid.pageobjects.page_object import PageObject
from seleniumtid.pageelements import InputText, PageElement
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from page_objects.project_card import ProjectCardPageObject


class ProjectsPageObject(PageObject):

    def init_page_elements(self):
        self.main_title = PageElement(By.CSS_SELECTOR, 'header.main-title')
        self.view_apps = PageElement(By.ID, 'button-view-apps')

        self.create_project_card = PageElement(By.CSS_SELECTOR, 'li.create-project')
        self.project_name_input = InputText(By.CSS_SELECTOR, 'li.create-project input.tc-input')
        self.create_project_button = PageElement(By.ID, 'button-create-project')
        self.error_message = PageElement(By.CSS_SELECTOR, 'li.create-project div.tc-message')

        self.cards = PageElement(By.CSS_SELECTOR, 'li.card:not(.create-project)')

    def create_new_project(self, project_name):
        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(self.create_project_card.locator))
        self.project_name_input.element().clear()
        self.project_name_input.text = project_name
        self.create_project_button.element().click()
        return ProjectCardPageObject(project_name)

    def project_cards(self):
        card_elements = self.driver.find_elements(*self.cards.locator)
        return [ProjectCardPageObject(card.get_attribute("data-project-name")) for card in card_elements]

    def length_project_cards(self):
        return len(self.driver.find_elements(*self.cards.locator))

    def delete_all_projects(self):
        [card.delete_project() for card in self.project_cards()]

    # TODO - Implement
    def wait_for_first_project_card(self):
        '''
        Method that ensures the presence of the tip card, when we're going to create the first project
        '''
        pass
