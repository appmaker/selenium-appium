# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

from seleniumtid.pageobjects.page_object import PageObject
from seleniumtid.pageelements import PageElement
from selenium.webdriver.common.by import By
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchElementException
from page_objects.projects import ProjectsPageObject
from page_objects.not_logged import NotLoggedPageObject


class StartPageObject(PageObject):

    def init_page_elements(self):
        self.start_button = PageElement(By.ID, 'button-start')
        self.view_apps_button = PageElement(By.ID, 'button-view-apps')
        self.feedback_button_no = PageElement(By.CSS_SELECTOR, 'div.feedback-dialog button.subdued')
        self.username = self.config.get('Github', 'username')
        self.passwd = self.config.get('Github', 'password')
        self.accept_cookies = PageElement(By.CSS_SELECTOR, '#cookies-warn button')

    def open(self):
        self.driver.get(self.config.get('Common', 'url'))
        # Try-except this line because we can call this method more than once in a single test, and at taht point,
        # the feedback button will not be visible any more.
        try:
            self.feedback_button_no.element().click()
        except ElementNotVisibleException:
            pass
        # Click the "Accept cookies" button, if found, so it's not there anymore
        try:
            self.accept_cookies.element().click()
        except NoSuchElementException:
            pass

    def view_projects(self, expect_authentication=True):
        '''Show the list of projects.

        expect_authentication: indicates if we should expect the "not logged" error message. This is useful
        in executions where more than one test exists in the same file. Only the first one triggers the error,
        since the authentication remains valid for the rest of the driver life.'''

        self.start_button.element().click()
        not_logged_page = NotLoggedPageObject()

        if expect_authentication:
            github_login_page = not_logged_page.click_auth_button()
            github_login_page.enter_login_data(self.username, self.passwd)
        return ProjectsPageObject()
