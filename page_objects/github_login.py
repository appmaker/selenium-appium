# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
from seleniumtid.pageobjects.page_object import PageObject
from seleniumtid.pageelements import InputText, PageElement
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from seleniumtid import selenium_driver


class GitHubLoginPageObject(PageObject):

    def init_page_elements(self):
        self.username = PageElement(By.CSS_SELECTOR, '#login_field')
        self.password = PageElement(By.CSS_SELECTOR, '#password')
        self.signin_button = PageElement(By.CSS_SELECTOR, 'input.btn[name=commit]')
        self.wait_time = int(selenium_driver.config.get('Common', 'explicitly_wait'))

    def enter_login_data(self, username, passwd):
        WebDriverWait(self.driver, self.wait_time).until(EC.visibility_of_element_located(self.username.locator))
        self.username.element().send_keys(username)
        self.password.element().send_keys(passwd)
        self.signin_button.element().click()
