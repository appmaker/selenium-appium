# -*- coding: utf-8 -*-

u"""
(c) Copyright 2014 Telefónica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefónica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefónica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
"""

from page_element import PageElement
from text_page_element import Text
from input_text_page_element import InputText
from select_page_element import Select
from button_page_element import Button

__all__ = ['PageElement', 'Text', 'InputText', 'Select', 'Button']
