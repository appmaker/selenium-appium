.. seleniumtid documentation master file, created by
   sphinx-quickstart on Mon Mar 31 17:27:40 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to seleniumtid's documentation!
=======================================

Contents:

.. toctree::
   :maxdepth: 4

   seleniumtid

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`